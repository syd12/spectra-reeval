/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.graphobjects;

/**
 *
 * @author sydgillani
 */
public class Edge<V> {

    
    public V from, to;
   // public int weight;

    public Edge(V from, V to) {
        if (from == null || to == null) {
            throw new IllegalArgumentException("null");
        }
        this.from = from;
        this.to = to;
       // this.weight = weight;
    }


    public String toString() {
        return "<" + from + ", " + to + ",  >";
    }
    public V getFrom() {
        return from;
    }

    public V getTo() {
        return to;
    }
}