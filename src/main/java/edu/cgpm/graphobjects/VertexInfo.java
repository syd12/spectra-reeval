/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.graphobjects;

/**
 *
 * @author sydgillani
 */
public class VertexInfo<V> {
    /** The vertex itself. */
    public V v;

    /** A mark for whether this vertex has been visited.  Useful for path searching. */
    public boolean visited;
    
    /** Constructs information for the given vertex. */
    public VertexInfo(V v) {
        this.v = v;
        this.clear();
    }
    
    /** Resets the visited field. */
    public void clear() {
        this.visited = false;
    }
}