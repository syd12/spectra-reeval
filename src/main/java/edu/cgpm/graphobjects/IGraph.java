/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.graphobjects;

import com.google.common.collect.Multimap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sydgillani
 */
public interface IGraph<V> {
    public void addVertex(V v);
    
    public void addEdge(V v1, V v2);
    
    public boolean hasEdge(V v1, V v2);
    
    public Edge<V> getEdge(V v1, V v2);
    
    public boolean hasPath(V v1, V v2);

    public List<V> getDFSPath(V v1, V v2);
    
    public String toString();
      public List<Edge<V>> getTheEdgeList(V from);
         public List<V> gettheRList(V v1);
         
       
           public List<V> getDFSResult(V v1, List<V> path);
            public void deleteVertix(V v);
            
              public List<V> getResult2(V v1, List<V> path);
               public void getDFStest(V v,Multimap<V,V> list );
}
