/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.graphobjects;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sydgillani
 */
public class DFS<V> {
        private Map<V, VertexInfo<V>> vertexInfo;     // marked[v] = is there an s-v path?
        
        private Map<V, List<Edge<V>>> adjacencyList; 
   // private int[] edgeTo;        // edgeTo[v] = last edge on s-v path
    private final int s;         // source vertex
    
    public DFS(int s,  Map<V, List<Edge<V>>> aL){
         this.adjacencyList = aL;
        this.vertexInfo = new HashMap<V, VertexInfo<V>>();
        this.s=s;
    }

}
