/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.UI;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.query.helpers.QueryDiscriptorOptimised;
import edu.cgpm.query.parser.QueryParser;
//import edu.telecomstet.cgpm.controllers.EventCountController;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.List;
import java.util.Set;
import javax.xml.datatype.DatatypeConfigurationException;
import org.antlr.runtime.RecognitionException;
import org.semanticweb.yars.nx.parser.NxParser;

/**
 *
 * @author sydgillani
 */
public class EngineCommandLineDEP {
    
//        public NxParser nxp2; 
//        public QueryDiscriptorOptimised descriptor2;
//        public   DictionaryOpImpl dictimpl2;
//    
//        
//      
//        
//      public static void main(String args[]) throws RecognitionException, ParseException, DatatypeConfigurationException, Exception{
//      
//          /**
//           * The Input consist of a query, data file with the KG streams, a window attribute in Hours, Minutes, Seconds,
//           * and an attribute telling if the event-based engine is used or incremental-based engine is used, 
//           * regarding the events, for event-based, one should specify the number of triples within an event, or if using a quad file, 
//           * then describe the named-graph option.
//           *
//           */
//          
//        int nT=Integer.parseInt(args[0]);
//        String q="";
//        switch (nT){
//            case 1:
//                 q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        
//                        "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
//                        
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                         + "WHERE   "
//                         + " SEQ (A) "
//                           + "{"
//                + " DEFINE GPM A ON S1 "
//          
//                  +" { "
//                        +" ?person sioc:creator_of ?post. "
//                          +" ?post sib:hashtag ?tag. "
//                          
//                      + " Filter (?tag = 'Battle'). } "
//                      
//                             
//                          
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//                + "   }";
//                break;
//                
//            case 2:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        
//                        "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
//                        
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                         + "WHERE   "
//                         + " SEQ (A) "
//                           + "{"
//                + " DEFINE GPM A ON S1 "
//          
//                  +" { "
//                        +" ?person sioc:creator_of ?post. "
//                          +" ?post sib:hashtag ?tag. "
//                        +" ?post sib:browser ?brow. "
//                         +" ?post sib:agent ?agent. "
//                          
//                       + " Filter (?tag = 'Battle'). "
//                        + " } "
//                      
//                             
//                          
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//                + "}";
//                break;
//                
//                
//            case 3:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                       "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix snvoc: <http://www.ldbc.eu/ldbc_socialnet/1.0/vocabulary/>  " +
//                        "prefix sn: <http://www.ldbc.eu/ldbc_socialnet/1.0/data/> " +
//                        
//                        "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
//                        
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                         + "WHERE   "
//                         + " SEQ (A) "
//                           + "{"
//                + " DEFINE GPM A ON S1 "
//          
//                  +" { "
//                        + " ?forum snvoc:hasCreator ?mod. "
//                        + " ?mod snvoc:likes ?like. " +
//                        "  ?like snvoc:hasPost ?post. "
//                        + " ?post snvoc:hasTag ?tag. "
//                        + "  ?comt snvoc:replyOf ?post. "
//                        + " ?post snvoc:isLocatedIn ?loc. "
//                
//                        + "  } "
//                      
//                             
//                          
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//                + "}";
//                break;
//                
//                
//                 case 4:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        "prefix sibo: <http://swat.cse.lehigh.edu/onto/univ-bench.owl#>"+
//                        
//                        " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
//                        
//                         " prefix dc: <http://purl.org/dc/terms/> "+
//                        "prefix dc2: <http://purl.org/dc/elements/1.1/>"+
//                        " prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        " prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                   + "WHERE   "
//                     
//                  +" { "
//                        +"?uni rdf:type ?course."
//                        +"?course sibo:name ?name."
//                        +"?name sibo:name2 ?fullname."
//                        + " ?name sibo:name3 ?course. } ";
//                      
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//               
//                break;
//                     
//                     case 5:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                         "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        
//                        " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
//                        " prefix ub: <http://swat.cse.lehigh.edu/onto/univ-bench.owl#> "+
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                   + "WHERE  { "
////                     
////               +"  ?x ub:advisor ?y. "
////                        + " ?x rdf:type ub:UndergraduateStudent. "
////                        + "  ?x ub:takesCourse ?z.  "
////                        + "?y ub:teacherOf ?z. " + //3
////"       ?y rdf:type ub:FullProfessor. " +//4
////"       ?z rdf:type ub:Course. " +//2
////
////"      } ";   ///1
////                      
////                   //  + " Filter (?tag2 = ?tag). }"
////                             
// +"  ?y ub:teacherOf ?z. " + //0
//           "    ?x ub:takesCourse ?z.   " +//3    
//                        
//                      
//                         "       ?z rdf:type ub:Course.  " +//1
//
//             " ?x ub:advisor ?y. " + //5 
//
//             "   ?y rdf:type ub:FullProfessor. " +//2    
//                       
//
//    "       ?x rdf:type ub:UndergraduateStudent. " + //4
//                      
//
//
//
//                        
//    
//                 
//
//"        } ";   ///1
//                          
//                         
//                     
//               
//                break;
//                                 
//                     case 6:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                         "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        
//                        " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  "+
//                        " prefix ub: <http://swat.cse.lehigh.edu/onto/univ-bench.owl#> "+
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                   + "WHERE  { "
//                     
//               +"   ?x ub:worksFor ?uu. " +
//"         ?x rdf:type ub:FullProfessor. " +
//"         ?x ub:name ?y1. " +
//"        ?x ub:emailAddress ?y2. " +
//"         ?x ub:telephone ?y3.   }  ";
//                      
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//               
//                break;
//                
//        }
//        
//        
//        
//        
//        
//        
//       /**
//        * Initiate the Dictionary and parse the input Query.
//        */
//        DictionaryOpImpl dictimpl = new DictionaryOpImpl();
//        
//        final QueryDiscriptorOptimised descriptor = QueryParser.parse(q, dictimpl,"");//"/Users/sydgillani/Desktop/dataSet/Output_Static.nt");
//        
//        /**
//         * Open the input File with custom NX parser.
//         */
//         FileInputStream is = new FileInputStream(args[1]);//"/Users/sydgillani/Documents/CGPMworkspace/ContinuousGPM/src/main/java/edu/telecomstet/testData/SocialNetwork-Sample-data.nq");//args[0]);//"/Users/sydgillani/Desktop/dataSet/test.n3");//"/Users/sydgillani/NetBeansProjects/IntellCEPNFA/src/main/resources/data/alldata.n3");//"/Users/sydgillani/Desktop/dataSet/Output.nq");
//         NxParser nxp = new NxParser(is);
//         
//         /**
//          * Initialise the Engine, depending on the input choice.
//          */
//         EventCountController ec = new EventCountController (nxp,    19);
//         ec.setNFADataList( descriptor.getNfaData());
//         ec.setDict(dictimpl);
//         ec.runEngine();
//      }
//      
//      
//  public void forTests(int queryId, String fileName) throws FileNotFoundException, RecognitionException, ParseException, DatatypeConfigurationException{
//       int nT=queryId;
//        String q="";
//        switch (nT){
//            case 1:
//                 q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        
//                        "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
//                        
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                         + "WHERE   "
//                         + " SEQ (A) "
//                           + "{"
//                + " DEFINE GPM A ON S1 "
//          
//                  +" { "
//                        +" ?person sioc:creator_of ?post. "
//                          +" ?post sib:hashtag ?tag. "
//                          
//                      + " Filter (?tag = 'Battle'). } "
//                      
//                             
//                          
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//                + "   }";
//                break;
//                
//            case 2:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        
//                        "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
//                        
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                         + "WHERE   "
//                         + " SEQ (A) "
//                           + "{"
//                + " DEFINE GPM A ON S1 "
//          
//                  +" { "
//                        +" ?person sioc:creator_of ?post. "
//                          +" ?post sib:hashtag ?tag. "
//                        +" ?post sib:browser ?brow. "
//                         +" ?post sib:agent ?agent. "
//                          
//                       + " Filter (?tag = 'Battle'). "
//                        + " } "
//                      
//                             
//                          
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//                + "}";
//                break;
//                
//                
//            case 3:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                       "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix snvoc: <http://www.ldbc.eu/ldbc_socialnet/1.0/vocabulary/>  " +
//                        "prefix sn: <http://www.ldbc.eu/ldbc_socialnet/1.0/data/> " +
//                        
//                        "prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>"+
//                        
//                        "prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        "prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                         + "WHERE   "
//                         + " SEQ (A) "
//                           + "{"
//                + " DEFINE GPM A ON S1 "
//          
//                  +" { "
//                        + " ?forum snvoc:hasCreator ?mod. "
//                        + " ?mod snvoc:likes ?like. " +
//                        "  ?like snvoc:hasPost ?post. "
//                        + " ?post snvoc:hasTag ?tag. "
//                        + "  ?comt snvoc:replyOf ?post. "
//                        + " ?post snvoc:isLocatedIn ?loc. "
//                
//                        + "  } "
//                      
//                             
//                          
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//                + "}";
//                break;
//                
//                
//                 case 4:
//                q=" prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> " +
//                        "prefix xsd: <http://www.w3.org/2001/XMLSchema#>  " +
//                        "prefix sib: <http://www.ins.cwi.nl/sib/vocabulary/> " +
//                        "prefix sioc: <http://rdfs.org/sioc/ns#> " +
//                        "prefix sibo: <http://swat.cse.lehigh.edu/onto/univ-bench.owl#>"+
//                        
//                        " prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "+
//                        
//                         " prefix dc: <http://purl.org/dc/terms/> "+
//                        "prefix dc2: <http://purl.org/dc/elements/1.1/>"+
//                        " prefix foaf: <http://xmlns.com/foaf/0.1/> " +
//                        " prefix dbpedia: <http://dbpedia.org/resource/>  "
//                               + " SELECT ?tag "+
//                         " WITHIN 1000000 SECONDS "+
//                "FROM STREAM S1 <http://example.org/main>  "
//               
//                         + "WHERE   "
//                     
//                  +" { "
//                        +"?uni rdf:type ?course."
//                        +"?course sibo:name ?name."
//                        +"?name sibo:name2 ?fullname."
//                        + "  } "
//                      
//                             
//                          
//                   //  + " Filter (?tag2 = ?tag). }"
//                             
//                          
//                         
//                     
//                + "";
//                break;
//                
//        }
//        
//        
//        
//        
//        
//        
//       /**
//        * Initiate the Dictionary and parse the input Query.
//        */
//        dictimpl2 = new DictionaryOpImpl();
//        
//        descriptor2 = QueryParser.parse(q, dictimpl2,"");//"/Users/sydgillani/Desktop/dataSet/Output_Static.nt");
//        
//        /**
//         * Open the input File with custom NX parser.
//         */
//         FileInputStream is = new FileInputStream(fileName);//"/Users/sydgillani/Documents/CGPMworkspace/ContinuousGPM/src/main/java/edu/telecomstet/testData/SocialNetwork-Sample-data.nq");//args[0]);//"/Users/sydgillani/Desktop/dataSet/test.n3");//"/Users/sydgillani/NetBeansProjects/IntellCEPNFA/src/main/resources/data/alldata.n3");//"/Users/sydgillani/Desktop/dataSet/Output.nq");
//         nxp2 = new NxParser(is);
//         
//  }    
//  
//  
//  public static  void testGuvana(){
//      Set<String> colors = ImmutableSet.of("Silver", "White", "Grey", "Purple");
//Set<String> fuelTypes = ImmutableSet.of("Petrol", "Hybrid");
//Set<String> brands = ImmutableSet.of("Toyota", "Micro");
//
//Set<List<String>> result = Sets.cartesianProduct(colors, fuelTypes, brands);
//System.out.println(result);
//  }
//      
}
