/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.queryJoins;

import edu.cgpm.qj.interfaces.QueryProcCycleInterface;
import com.google.common.collect.Multimap;
import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.MultiBidirectionalIndex;
import edu.cgpm.datastructure.SO;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.rulesmodel.Dependability;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

/**
 *
 * @author sydgillani
 */
public class QueryProcessorCycle implements QueryProcCycleInterface {
    
    
    /***
     * TODO: change of jdd, change of RC2, change of output, etc
     */
    private final ResultManipulation _reM;
    private  final Utilities Util= new Utilities();
    private final IncQueryProcessingCycle incQ= new  IncQueryProcessingCycle();
   
public QueryProcessorCycle(BlockingQueue<String> rq){
    _reM = new  ResultManipulation();
      _reM.setResultqueue(rq);
    
}
    public void runQueryProcessing(Collection<AutomataState> a, DictionaryOpImpl dic,  int change, FinalView[] _FV, int evalType){
      
        
      
        if(evalType==1){  ///used event-based evalutaion
            this.eventBasedEval(a, dic, change);
        }else{  ///use incremental evalution
            this.runFirstPhase(a, dic, change, _FV, 0, 0, 0);
        }
        
        
    }
     
    
   public void eventBasedEval(Collection<AutomataState> a, DictionaryOpImpl dic,  int change){
           if(change == 1 ){
              Util.refresh();
        // if(proceed==0){
             //  System.out.println("IN HERE");
           //  Util.stop=0;
        
        List<AutomataState> aStates= a.stream().filter(x->x.getRule().getC2().size()>0 ).collect(Collectors.toList());
            
            
            
            if( a.size()==aStates.size()){
                ////run the joins 
                ///if the joins produces the results then put it in FV as usual
               eventStage(aStates, dic);
            }
           }
   } 
    
   
   private void eventStage( List<AutomataState> aStates, DictionaryOpImpl dic){
         refreshStructures(aStates);
        
        tpJoinAutomataStage(aStates); ///execute all the joins
         
        if(Util.stop==0){///if all the joins have produced the results
        this.eventResults(aStates, dic); ////how to extract the results and vice versa, still pain in the fucking arse.
          
        }
   }
    
    @Override
    public void runFirstPhase(Collection<AutomataState> a, DictionaryOpImpl dic,  int change, FinalView[] _FV, long timestamp,long range, long step){
     
        
        if(change == 1){
              Util.refresh();
        // if(proceed==0){
     
           //  Util.stop=0;
        
        List<AutomataState> aStates= a.stream().filter(x->x.getRule().getC2().size()>0 ).collect(Collectors.toList());
            
            
            
            if(_FV[0].getR().isEmpty() && a.size()==aStates.size()){
                ////run the joins 
                ///if the joins produces the results then put it in FV as usual
              initialStage(aStates, dic, _FV, timestamp, range, step);
            }else if(!_FV[0].getR().isEmpty()){
                ////join all the triple patterns with c.size greater than zero
                ///if the all the joins produces the results then well done and if the size is equal to the automata size put it in FV
                ///else join only few of them produces the results, then take the one with R>0 and join them with FV
                //if the above list is equal to zero then send only the one with new C's to have a fun with FV
               // this.removeOldTriples(aStates, timestamp, range, step); ///remove the shity old triples first
                
                this.generalStage(a.size(), aStates, dic, _FV, timestamp, range, step);
            }
        }
    }
    @Override
     public void generalStage(int size,List<AutomataState> aStates, DictionaryOpImpl dic, FinalView[] _FV, long timestamp,long range, long step){
        
         refreshStructures(aStates); ///May be remove this for triple streams
         
         if(aStates.size()==1){
             incQ.run(aStates,_FV,dic,timestamp,range,step, Util,_reM);
         }else{
         
         
         tpJoinAutomataStage(aStates); ///execute all the joins
         
        if( Util.stop==0 && aStates.size() == size){///if all the joins have produced the results
        // outputResultsAll(aStates, dic,_FV,timestamp,1);
             outputResultsAll(aStates, dic,_FV,timestamp);
        }else if(Util.stop==0){
            ///simple send them to the incermenal matching stuff
            // incQ.run(aStates,_FV,indexCount,dic,timestamp,range,step);
             incQ.run(aStates,_FV,dic,timestamp,range,step, Util,_reM);
        }else if(Util.stop==1){
            ///Get all the new ones and send it for the joins between them,(should change it later), and send them for the incremental processing. A complex case though
            //Before send it for the joins you can also check it 
             Util.stop=0;
            aStates = aStates.stream().filter(x->x.getChange()==1 ).collect(Collectors.toList());
             refreshStructures(aStates);
             if(!aStates.isEmpty() && aStates.size()> 1){
                 tpJoinAutomataStage(aStates);
                 
                 
                 if(Util.stop==0){
                      incQ.run(aStates,_FV,dic,timestamp,range,step, Util,_reM);
                    // incQ.run(aStates,_FV,indexCount,dic,timestamp,range,step);
                 }
                 
                 
                 ///over here if stop==1 refresh the data structure and send it to the incremental process again, for the cc join
             }else{
                  incQ.run(aStates,_FV,dic,timestamp,range,step, Util,_reM);
                // incQ.run(aStates,_FV,indexCount,dic,timestamp,range,step);
             }
        }
         }
    }
    
     /////////////////////////////////////////////////////////////////////////
     
    @Override
         public void removeOldTriples(List<AutomataState> as, long time, long range, long step){
        
        List<AutomataState> _fas= as.stream().filter(x -> x.getRule().getC2().size() > 0 ).collect(Collectors.toList());
        
         long tb =  (time - range) / step;
         
         tb = (long) Math.floor(tb);
         for(AutomataState rs:_fas){
             this.tripleRemoval(rs, tb);
         }
    }
    
    @Override
    public void tripleRemoval(AutomataState s, long tb){
        ////get all the timestamps from the list and sort it
        
       List<Long> tobeRemoved = s.getRule().getTimeSOpairs().keySet().stream().sorted().collect(Collectors.toList());
  if(tobeRemoved.get(0)<tb){
  tobeRemoved=tobeRemoved.stream().filter(x->x<tb).collect(Collectors.toList());
    
       if(!tobeRemoved.isEmpty()){
      Collection<SO> soTobeRemoved= s.getRule().getTimeSOpairs().removeAll(tobeRemoved);
       
        for (SO so:soTobeRemoved) {
            s.getRule().getC2().remove(so.getSub(), so.getPred());
        }
       }
}
    }
     
     /////////////////////////////////////////////////////////////////////////
     
     
    @Override
     public void initialStage( List<AutomataState> aStates, DictionaryOpImpl dic,  FinalView[] _FV, long timestamp,long range, long step){
       ///Remove the earlier results
        
        refreshStructures(aStates);
        
        tpJoinAutomataStage(aStates); ///execute all the joins
         
        if(Util.stop==0){///if all the joins have produced the results
        outputResultsAll(aStates, dic,_FV,timestamp); ////how to extract the results and vice versa, still pain in the fucking arse.
          
        }
        
    }
    
    
    @Override
    public void refreshStructures( List<AutomataState> aStates){
         for(AutomataState s:aStates){
               
            //  
             //   System.out.println(s.getStateId()+"    "+s.getRule().getC2().size());
                
               if(!s.getEdge().getRreal().getR().isEmpty()){
                        s.getEdge().getRreal().getR().clear();//.getR().clear();
                        s.getEdge().getRreal().getrIndexUpdated().clear();//rIndexClear();
                        s.getEdge().getRreal().getParentChild().clear();
                    }else if(!s.getEdge().getrCopied().getR().isEmpty()){
                        s.getEdge().getrCopied().getR().clear();//rClear();
                        s.getEdge().getrCopied().getrIndexUpdated().clear();//rIndexClear();
                        s.getEdge().getrCopied().getParentChild().clear();
                    }
               
               if(s.getRule().getDepends().size()>1){
                   s.getRule().getDepends().get(0).setJoined(0);
                   s.getRule().getDepends().get(1).setJoined(0);
               }else{
                   s.getRule().getDepends().get(0).setJoined(0);
               }
            }
         
         if(!Util._joinOrder.isEmpty())
         Util._joinOrder.clear();
    }
    
    
    
    
    /*
     public boolean run(Automata a, DictionaryOpImpl dic, int go, int change){
         this.dic=dic;
         
         this.breakpt=1000000;
        // if(proceed==0){
        indexCount=0;  ///change it for the incremental ones
        if(go == 1 && change == 1){
            ///Join over here
            
            stop=0;
            
            
            
            for(AutomataState s:a.getStates()){
                 if(!s.getEdge().getRreal().getR().isEmpty()){
                        s.getEdge().getRreal().getR().clear();//.getR().clear();
                        s.getEdge().getRreal().getrIndexUpdated().clear();//rIndexClear();
                    }else if(!s.getEdge().getrCopied().getR().isEmpty()){
                        s.getEdge().getrCopied().getR().clear();//rClear();
                        s.getEdge().getrCopied().getrIndexUpdated().clear();//rIndexClear();
                    }
                 
                 
                 if(!s.getParentChild().isEmpty()){
                     s.getParentChild().clear();
                 }
                    if(s.getRule().getDepends().size()>1){
                   s.getRule().getDepends().get(0).setJoined(0);
                   s.getRule().getDepends().get(1).setJoined(0);
               }else{
                   s.getRule().getDepends().get(0).setJoined(0);
               }
                 
             
            }
            
            
        //    tpJoinAutomataStage1(aList.get(i),dic);   ///send it for the join, decide about the joins in there, not as a new functions
            
         // tpJoinAutomataStage(a,dic);
            // System.out.println("STOPPING " + stop);
        if(stop==0){
         outputResultsAll(a, dic);
        } else {  //incremental join part
                
                }
       
    }
         return false;
     }
    */
    @Override
     public void outputResultsAll(List<AutomataState> a,DictionaryOpImpl dic, FinalView[] _FV, long timestamp ){
           Set<Indexes> keys=null;
           Util.setList.clear();
          // this.stateID=4;
        if(!a.get(Util.stateID).getEdge().getRreal().getR().isEmpty()){
          
             keys=a.get(Util.stateID).getEdge().getRreal().getrIndexUpdated().keySet();
             

        }else if(!a.get(Util.stateID).getEdge().getrCopied().getR().isEmpty()){
           
             keys=a.get(Util.stateID).getEdge().getrCopied().getrIndexUpdated().keySet();
        }
        
     
      
      for(Indexes in:keys){
          // statesDFS(a.getStates().get(stateID), a, in);
         // this.testExtraction(in, a);
          
          //_resultExtraction(in,a.getStates());
          
      _reM .resultExtraction_Case2(in, a,_FV,timestamp,Util);
       _reM.start(Util.visited, dic);
//         _reM. printOut(Util.setList,dic);
//          Util.setList.clear();
      }  
      
      
      // Util._joinOrder.clear();  ////..noned to doit
     }
   
   private void eventResults(List<AutomataState> a,DictionaryOpImpl dic ){
               Set<Indexes> keys=null;
           Util.setList.clear();
          // this.stateID=4;
        if(!a.get(Util.stateID).getEdge().getRreal().getR().isEmpty()){
          
             keys=a.get(Util.stateID).getEdge().getRreal().getrIndexUpdated().keySet();
             

        }else if(!a.get(Util.stateID).getEdge().getrCopied().getR().isEmpty()){
           
             keys=a.get(Util.stateID).getEdge().getrCopied().getrIndexUpdated().keySet();
        }
        
     
      
      for(Indexes in:keys){
          // statesDFS(a.getStates().get(stateID), a, in);
         // this.testExtraction(in, a);
          
          //_resultExtraction(in,a.getStates());
          
         _reM .resultExtraction_Event(in, a, Util);
         _reM.start(Util.visited, dic);// printOut(Util.setList,dic);
         
      }  
   }
    
    @Override
    public Set<SO> extractResults(AutomataState s, AutomataState js, Indexes index, Set<SO> set, FinalView[] _FV, long timestamp){
   Set<SO> newSet = new HashSet<>();
        if(set.isEmpty()){
            
            
            
            ///get it from S and 
            
            if(!s.getEdge().getRreal().getR().isEmpty()){
                
                
            
                         
                         this.firstStage(s.getEdge().getRreal().getrIndexUpdated(), index, s, _FV, timestamp, set);
                 // setList.add(set);
              //    System.out.println(set);
                secondStage(s, js, newSet, set,_FV,timestamp);
                
                
                
            }else if(!s.getEdge().getrCopied().getR().isEmpty()){
                
                    
                        
                        firstStage(s.getEdge().getrCopied().getrIndexUpdated(), index, s, _FV, timestamp, set);
                          //  System.out.println(set);
                         secondStage(s, js, newSet, set,_FV,timestamp);
                   // return set;
                    
               
            }
           
     }else{
         ///go around the set and get the childeren from the results set and then get the values from join state
         
          secondStage(s, js, newSet, set,_FV,timestamp);
     }
        
      return newSet;  
    }
    
    
    @Override
    public void firstStage(MultiBiMap result, Indexes index, AutomataState s, FinalView[] _FV, long timestamp,Set<SO> set){
        
                         for (Iterator<Entry<Indexes, SO>> it = result.entries().iterator(); it.hasNext();) {
                             Entry<Indexes,SO> sp = it.next();
                             if(sp.getKey().getGlobalIndex()==index.getGlobalIndex()){
                                 
                             //    System.out.println("State for FV: "+s.getStateId());
                                 _FV[s.getStateId()].getR().put(sp.getValue().getSub(), sp.getValue().getPred());
                                 _FV[s.getStateId()].getTimeSPpairs().put(timestamp, sp.getValue());   ///temporal aspects like time tree should be captured over here
                               
                                   _FV[s.getStateId()].getrIndex().put(index.getGlobalIndex(), sp.getValue());
                                     s.getRule().getC2().remove(sp.getValue().getSub(), sp.getValue().getPred());
                                set.add(sp.getValue());
                             }
                         }
                         
                         Util.setList.add(set);
    }
    
    @Override
     public void secondStage(AutomataState s, AutomataState js,Set<SO> newSet, Set<SO> set, FinalView[] _FV, long timestamp){
         
        // Set<SP> toBeRemoved= new HashSet<>();
         for(SO sp: set){
             /////get the parent from s
             
             
             Set<Indexes> kin=null;
             if(!s.getEdge().getRreal().getR().isEmpty()){
                 kin = s.getEdge().getRreal().getrIndexUpdated().getKeys(sp);
             }else if (!s.getEdge().getrCopied().getR().isEmpty()){
                 kin = s.getEdge().getrCopied().getrIndexUpdated().getKeys(sp);
             }
             
             
             for(Indexes k :kin){
                 ///get from js
                 //get the parent from
                 
                 
                 
                 for(Indexes d:  s.getParentChild().get(k)){
                     
                     if(!js.getEdge().getRreal().getR().isEmpty()){
                        // newSet.addAll(js.getEdge().getRreal().getrIndexUpdated().get(d)); ///if not then remove it
                         ///add the FV stuff over here
                         
                         for(SO so:js.getEdge().getRreal().getrIndexUpdated().get(d)){
                             // System.out.println("State for FV: "+js.getStateId());
                              _FV[js.getStateId()].getR().put(so.getSub(), so.getPred());
                                 _FV[js.getStateId()].getTimeSPpairs().put(timestamp, so);   ///temporal aspects like time tree should be captured over here
                               
                                   _FV[js.getStateId()].getrIndex().put(d.getGlobalIndex(), so);
                                     js.getRule().getC2().remove(so.getSub(), so.getPred());
                             
                             newSet.add(so);
                         }
                         
                         
                         ///from the other set as well
                     }else if (!js.getEdge().getrCopied().getR().isEmpty()){
                         
                         if(!js.getEdge().getrCopied().getrIndexUpdated().get(d).isEmpty()){
                              for(SO so:js.getEdge().getrCopied().getrIndexUpdated().get(d)){
                                   _FV[js.getStateId()].getR().put(so.getSub(), so.getPred());
                                 _FV[js.getStateId()].getTimeSPpairs().put(timestamp, so);   ///temporal aspects like time tree should be captured over here
                               
                                   _FV[js.getStateId()].getrIndex().put(d.getGlobalIndex(), so);
                                     js.getRule().getC2().remove(so.getSub(), so.getPred());
                                     
                                      newSet.add(so);
                              }
                         }
                         
                         
                         
                     }
                     
                 }
                 
             }
             
             
             
         }
        /// set.removeAll(toBeRemoved);
        
     }
    @Override
 public void tpJoinAutomataStage(List<AutomataState> automata){
        
      
            
        
        for(AutomataState s:automata){
                tripleJoinStage(s,automata);
                
                if(Util.stop==1){
                   
                    return;
                }
            }
        
        
    }
    
       
    @Override
        public void tripleJoinStage(AutomataState currState, List<AutomataState> a){
        
        
           boolean bagIt=false;
         for (int i = 0; i < currState.getRule().getDepends().size(); i++) {
             Dependability dd= currState.getRule().getDepends().get(i);
             
             AutomataState sJoin = a.stream().filter(x->x.getStateId()==dd.getDependabilty_id()).findFirst().orElse(null);
              
             if(sJoin==null){
                 bagIt=true;
                    continue;
                }else if(i==0){
                    
                   
                  //  System.out.println("Curr State: "+currState.getStateId()+" Join State: "+sJoin.getStateId());
                    Util._joinOrder.add(currState.getStateId());
                    Util._joinOrder.add(sJoin.getStateId());
               
                }else if(i!=0 && bagIt){
                  //    System.out.println("Curr State: "+currState.getStateId()+" Join State: "+sJoin.getStateId());
                    Util._joinOrder.add(currState.getStateId());
                    Util._joinOrder.add(sJoin.getStateId());
                }
      //  for(Dependability dd:currState.getRule().getDepends()){
            if(dd.getJoined()==0  && (!currState.getRule().getC2().isEmpty() ||!currState.getEdge().getRreal().getR().isEmpty() || !currState.getEdge().getrCopied().getR().isEmpty())){
                ///find the join state
             //   AutomataState sJoin = a.getStates().stream().filter(x->x.getStateId()==dd.getDependabilty_id()).findFirst().get();
                //  resultHandler=true;
                ///check the type of the Join
//           Dependability jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==dd.getDependability_part()&& x.getDependability_part()==dd.getDependability_On()).findFirst().orElse(null);
//                if(jdd==null){
//                 
//                   continue;
//                    
//                }
              //   AutomataState sJoin = a.getStates().stream().filter(x->x.getStateId()==dd.getDependabilty_id()).findFirst().orElse(null);
              
                
                //if(i==0){
                //    System.out.println("Curr State: "+currState.getStateId()+" Join State: "+sJoin.getStateId());
              //  }
           
         //  dd.setOrderJoin(sJoin.getStateId());///uselesss
          // jdd.setOrderJoin(currState.getStateId());
           
         ///do stuff with join dd and dd
           
           ///from to 
              
               Dependability jdd =null; 
                //Remove the stop ==0, as we need tp join as many as possible
                if(dd.getDependability_On()==1 && dd.getDependability_part()==1 ){ ///stop==0;
                    
                    /// incrementalSSJoinStage1(sJoin,currState,a);
                      jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==1 && x.getDependability_part()==1).findFirst().orElse(null);
                    incrementalSSJoinStage(currState,sJoin,dd,jdd);
                    
                }else if(dd.getDependability_On()==1 && dd.getDependability_part()==0){
                    ///s for current stae
                    //  incrementalOSJoin(currState,sJoin,a);
                     jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==0 && x.getDependability_part()==1).findFirst().orElse(null);
                   incrementalOSJoinStage(currState, sJoin,dd,jdd);
                    
                }else if(dd.getDependability_On()==0 && dd.getDependability_part()==1 ){
                    //  incrementalSOJoin(currState,sJoin,a);
                      jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==1 && x.getDependability_part()==0).findFirst().orElse(null);
                   incrementalSOJoinStage(currState, sJoin,dd,jdd);
                }else if(dd.getDependability_On()==0 && dd.getDependability_part()==0 ){
                    
                      jdd =   sJoin.getRule().getDepends().stream().filter(x->x.getDependability_On()==0 && x.getDependability_part()==0).findFirst().orElse(null);
                    incrementalOOJoinStage(currState,sJoin,dd,jdd);
                }
                
                
             //   System.out.println("Current State: "+);
                
                 if(Util.stop==1){
                   
                    return;
                }
             //   System.out.println("STOP: "+ Util.stop);
                if(!currState.getEdge().getRreal().getR().isEmpty()){
                    ///then get 
                    
                    if(currState.getEdge().getRreal().getrIndexUpdated().size()<= Util.breakpt){
                       Util. breakpt=currState.getEdge().getRreal().getrIndexUpdated().size();
                       Util. stateID=currState.getStateId();
                    }
                    
                    
                    
                }else if (!currState.getEdge().getrCopied().getR().isEmpty()){
                    if(currState.getEdge().getrCopied().getrIndexUpdated().size()<= Util.breakpt){
                  Util. breakpt=currState.getEdge().getrCopied().getrIndexUpdated().size();
                    Util.stateID=currState.getStateId();
                    }
                }
                
                
                
                
                
                if(!sJoin.getEdge().getRreal().getR().isEmpty()){
                    ///then get 
                    
                    if(sJoin.getEdge().getRreal().getrIndexUpdated().size()<= Util.breakpt){
                      Util.breakpt=sJoin.getEdge().getRreal().getrIndexUpdated().size();
                      Util. stateID=sJoin.getStateId();
                    }
                    
                    
                    
                }else if (!sJoin.getEdge().getrCopied().getR().isEmpty()){
                    if(sJoin.getEdge().getrCopied().getrIndexUpdated().size()<= Util.breakpt){
                      Util. breakpt=sJoin.getEdge().getrCopied().getrIndexUpdated().size();
                       Util.stateID=sJoin.getStateId();
                    }
                }
                
                
                
                
                
            }
        }
        
        
        
    }
       
    
  
    @Override
    public void incrementalSSJoinStage(AutomataState currState,AutomataState joinState,Dependability dd,Dependability jdd){
        
        
        ///if the join state r and r join are empty
        
      
        
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
            
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    long index=Util.indexCount;
                
                if(currState.getRule().getC2().keySize()<joinState.getRule().getC2().keySize()){
                    
                    
                    
                    ///Send the index over here
                  
                    // Change
                    this.ssJoinOptimisedForCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),index,currState.getParentChild(),joinState.getParentChild());
                } else{
                    
                    
                    ///Change
                    this.ssJoinOptimisedForCC(joinState.getRule().getC2(),currState.getRule().getC2(),  joinState.getEdge().getRreal(), currState.getEdge().getRreal(),index,joinState.getParentChild(),currState.getParentChild());
                }
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    
                    this.ssJoinOptimisedForRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    
                    
                    
                    
                    
                    if(Util.checker){
                       
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    this.ssJoinOptimisedForRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    
                    
                    if(Util.checker){
                       
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                
                this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), currState.getEdge().getRreal(),joinState.getEdge().getrCopied() ,currState.getParentChild(),joinState.getParentChild());
                
                
                if(Util.checker){
                   
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndexUpdated().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    if(joinState.getEdge().getRreal().getR().keySize()<currState.getEdge().getRreal().getR().keySize()){
                        
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getRreal(), currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getEdge().getrCopied(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(joinState.getEdge().getRreal().getR().keySize()<currState.getEdge().getrCopied().getR().keySize()){
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getRreal(), currState.getEdge().getrCopied(), joinState.getEdge().getrCopied(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getrCopied(),joinState.getEdge().getRreal(), currState.getEdge().getRreal(),joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                
                if(Util.checker){
                   
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    if(joinState.getEdge().getrCopied().getR().keySize()< currState.getEdge().getRreal().getR().keySize()) {
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }
                    if(Util.checker){
                     
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    if(joinState.getEdge().getrCopied().getR().keySize()<currState.getEdge().getrCopied().getR().keySize()){
                        this.ssJoinOptimisedForRC2(joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        this.ssJoinOptimisedForRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    
                    if(Util.checker){
                        
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
            //      this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(), joinState.getEdge().getRreal(), currState.getEdge().getRreal());
            
            
            
            
        }
        
        
        
        
        
        
        
        //// check the tablesomappedcopied
        
        //TODO: remove the stop from the system
        ////////////////
        if(Util.checker)
        {
            dd.setJoined(1);
             if(jdd!=null){
            jdd.setJoined(1);
             }
           /// currState.setJoined(1);
           // joinState.setJoined(1);
            
          //  currState.setChange(0);
            //  automata.getStates().get(id).getRule().getTableSOMapped().removeAll(  automata.getStates().get(id).getEdge().getTableSOMapped()); //change it
            //   removeFromC(currState.getRule().getC(),currState.getEdge().getR());
            Util.checker=false;
            
        }else{
            Util.stop=1;
        }
        
        
        
        
        
        
        
        
        
        
    }
    
    
    
    
    
  ///Edit 1
    @Override
    public void ssJoinOptimisedForCC( MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2, long index2,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
        
        ///Take the index from the top
        ///Increment the index
       
        for(long key:_c1.keySet()){
            
            if(_c2.containsKey(key)){
                Util.checker=true;
               // this.indexes.clear(); ///dont do it every fucking time
               Util.parentChild.clear();
               // index=index+1;
                Set<Long> keyall=_c1.get(key);
                for(long val:keyall){
                     _r1.getR().put(key, val);
                     
                    
                   // _r1.getrIndex().put(indexCount, new SO(key,val));
                    Indexes kin= new Indexes(++Util.perTableIndex,++Util.indexCount);
                    _r1.getrIndexUpdated().put(kin, new SO(key,val));
                   Util.parentChild.add(kin);
                  //  this.indexes.add(indexCount);///delete it 
                   
                }
                
              
                
                Set<Long> r2val=_c2.get(key);
                for(long val:r2val){
                    
                      _r2.getR().put(key, val);

//                      
                      for(Indexes kin:Util.parentChild){
                          Indexes in= new Indexes(++Util.perTableIndex,kin.getGlobalIndex());
                           _r2.getrIndexUpdated().put(in, new SO(key,val));
                           
                           
//                           if(order==1){
                                _pC.put(kin, in);  //change it 
                                _jpC.put(in, kin);
//                           }else{
//                               _pC.put(in, kin);  //change it 
//                                _jpC.put(kin, in);
//                           }
                         
                          
                      }
                }                
            }
            //
        }
        
        
      this.eachJoinResult(_r1, _r2,_pC,_jpC);
    //  graphPairResult(dd);
    }
    
    
    ///Second and Fourth, and fifth Join
    
    /// chagen the multi BiMap tp multimap
   ///ISSUE: 2,4, 5
    @Override
    public void ssJoinOptimisedForRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3, MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
        
       // System.out.println("C1 "+ _c1.keySize());
       // System.out.println("R2 "+  _r2.getR().keySize());
      //  if(_c1.keySize()<_r2.getR().keySize()){
            
           //_C1 is a join state
         //   dd.clear();
            for(long key:_c1.keySet()){
                
                if(_r2.getR().containsKey(key)){
                      // this.indexes.clear();
                     Util.parentChild.clear();
                    ///first get the index value from _r
                    Util.checker=true;
                   
                    
                    
                    Set<Long> r2val=_r2.getR().get(key); //put all in the new r whcich is r3
                    for(long val:r2val){
                        _r3.getR().put(key,val);
                        // Set<Long> in = _r2.getrIndex().getKeys(new SO(key, val));
                         ///get the whole thing instead of just in
                         
                        Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(new SO(key, val));
                         
                        for(Indexes kin:ind){
                           // indexes.add(kin.getGlobalIndex());
                           Util.parentChild.add(kin);
                          _r3.getrIndexUpdated().put(kin, new SO(key,val));   
                        }
                        
//                         for(long kin:in){
//                             
//                             
//                             _r3.getrIndex().put(kin, new SO(key,val));
//                         }
                        
                       
                    }
                    
                    
                    ///now put the stuff in r3 which is for the join state
                    
                  
                    
                    Set<Long> c1val=_c1.get(key);
                    for(long val2:c1val){
                        
                          _r4.getR().put(key,val2);
//                     for(long in:indexes){
//                         ////create new indexes
//                        // long indexInc=indexCount+1;
//                       //  dd.put(in, ++indexCount);
//                         
//                         // dd.put(indexCount,in);/// remove this later
//                         ///add it to dd map
//                       // _r4.getrIndex().put(in, new SO(key,val2));
//                        _r4.getrIndexUpdated().put(new Indexes(++perTableIndex,in), new SO(key,val2));   
//                    }
//                     
                     
                     for(Indexes kin:Util.parentChild){
                         
                         Indexes in= new Indexes(++Util.perTableIndex,kin.getGlobalIndex());
                          _r4.getrIndexUpdated().put(in, new SO(key,val2));  
                          
//                          if(order==1){
                               _pC.put(in, kin);
                          _jpC.put(kin, in);
//                          }else{
//                              _pC.put(kin, in);
//                          _jpC.put(in, kin);
//                          }
                         
                     }
                    }  
                    
                    
                }
                //
            }
            
//        }else{
//            
//            
//            
//            for(long key:_r2.getR().keySet()){
//                
//                if(_c1.containsKey(key)){
//                    this.indexes.clear();
//                    
//                    this.parentChild.clear();
//                    ///first get the index value from _r
//                    checker=true;
//                    // _r2.getR().getValue(key);
//                    
//                  //  long index =0; //_r2.getrIndex().getKey(new SO(key, _r2.getR().getValue(key)));
//                    
//                    
//                    
//                    
//                    Set<Long> r2val=_r2.getR().get(key); //put all in the new r whcich is r3
//                    
//                    ///get the value from _r2.getindex
//                    for(long val:r2val)
//                        
//                    {   //index = _r2.getrIndex().getKey(new SO(key, val));///get keys
//                        _r3.getR().put(key,val);
////                        Set<Long> in2=_r2.getrIndex().getKeys(new SO(key, val));
///////get all index vales
////                        for(long kin:in2) 
////                        {
////                            this.indexes.add(kin);
////                            
////                            
////                            _r3.getrIndex().put(kin, new SO(key,val));
////                        }
//                           Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(new SO(key, val));
//                         
//                        for(Indexes kin:ind){
//                           // indexes.add(kin.getGlobalIndex());
//                            this.parentChild.add(kin);
//                          _r3.getrIndexUpdated().put(kin, new SO(key,val));   
//                        }
//                    }
//                    
//                    
//                    ///now put the stuff in r3 which is for the join state
//                    
//                    
//                    
//                    Set<Long> c1val=_c1.get(key);
//                    for(long val2:c1val){
//                        _r4.getR().put(key,val2);
//                        for(Indexes in:this.parentChild){
//                        
//                            
//                            Indexes ind= new Indexes(++perTableIndex,in.getGlobalIndex());
//                          //   dd.put(in, ++indexCount);
//                            //  dd.put(indexCount,in);/// remove this later
//                            _r4.getrIndexUpdated().put(ind, new SO(key,val2));
//                            _pC.put(in, ind);
//                            _jpC.put(ind, in);
//                            //_r4.getrIndex().put(in, new SO(key,val2));
//                        }
//                    }
//                }
//                //
//            }
//            
//        }
//        
        
        
        
        ///clear every thing in the R2
        //    _r2.getR().clear();
        //  _r2.getrIndex().clear();
        
      this.eachJoinResult(_r4, _r3,_pC,_jpC);
    
    //graphPairResult(dd);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    @Override
    public void ssJoinOptimisedForRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes,Indexes> _jpC){
        
        
        
        for(long key:_c1.getR().keySet()){
            
            if(_r2.getR().containsKey(key)){
             
                Util.checker=true;
                // _r2.getR().getValue(key);
                
               // long index = _r2.getrIndex().getKey(new SO(key, _r2.getR().getValue(key)));
                
                
               
                
                Set<Long> r2val=_r2.getR().get(key); //put all in the new r whcich is r3
                for(long val:r2val){
                    
                    
                    
                    SO sp= new SO(key,val);
                        
                        
                        Set<Indexes> in=_r2.getrIndexUpdated().getKeys(sp);
                    for(Indexes kin:in){
                        for(long val2:_c1.getR().get(key)){
                            
                            ///get the index and use it for the insertion procedure
                            SO sp2= new SO(key,val2);
                              Set<Indexes> in2=_c1.getrIndexUpdated().getKeys(sp2);
                             
                              for(Indexes rIndex:in2){
                                  if(rIndex.getGlobalIndex()== kin.getGlobalIndex()){
                                      _r3.getR().put(key,val);
                                      
                                      _r3.getrIndexUpdated().put(kin, sp);  ///wastage of insertion operation
                                     
                                      _r4.getrIndexUpdated().put(rIndex, sp2);
                                      _r4.getR().put(key,val2);
                                      
                                      _pC.put(rIndex, kin);
                                      
                                      _jpC.put(kin, rIndex);
                                      
                                  }
                              }
                              
//                            if(_c1.getrIndexUpdated().getKey(new SO(key,val2)).getGlobalIndex()==kin.getGlobalIndex()){
//                                _r3.getR().put(key,val);
//                                _r3.getrIndexUpdated().put(kin, sp);
//                                _pC.put(kin, _c1.getrIndexUpdated().getKey(new SO(key,val2))); ///Not required, so remove it
//                            }
                        }
                    }
                    
                     
                    
                     
                     
             
                     }
                
                
                ///now put the stuff in r3 which is for the join state
               // index = _c1.getrIndex().getKey(new SO(key, _c1.getR().getValue(key)));
                
               
//                
//                Set<Long> c1val=_c1.getR().get(key);
//                for(long val2:c1val){
//                    Set<Indexes> in=_c1.getrIndexUpdated().getKeys(new SO(key,val2));
//                    
//                    for(Indexes kin:in){
//                         for(long k2:r2val){
//                             if(_r2.getrIndexUpdated().getKey(new SO(k2,key)).getGlobalIndex()==kin.getGlobalIndex()){
//                                  _r4.getrIndexUpdated().put(kin, new SO(key,val2));
//                                   _r4.getR().put(key,val2);
//                                   _jpC.put(kin, _r2.getrIndexUpdated().getKey(new SO(k2,key)));///Not required, so remove it
//                             }
//                         }
//                    }
//                    
//                    
//                }
//                    
                    
                    
                    
                    
                    
              
                
            
            //
        }
        
        
      
    }
    
    
    }  
    
    
    
    
    
   
    
    
    
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    
   
    @Override
    public void incrementalSOJoinStage(AutomataState currState, AutomataState joinState,Dependability dd,Dependability jdd){
        
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
            
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                //  this.ssHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                long index=Util.indexCount;
                
                if(currState.getRule().getC2().size()<joinState.getRule().getC2().size()){
                    
                    
                    this.soHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),index,currState.getParentChild(),joinState.getParentChild());
                }else{
                    this.osHashJoinCC( joinState.getRule().getC2(),currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),index,joinState.getParentChild(),currState.getParentChild());
                }
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    ///reverse function
                    
                    this.osHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    if(Util.checker){
                        
                        
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    
                    //reverse function
                    this.osHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    
                    
                    if(Util.checker){
                      
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.soHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                
                
                if(Util.checker){
                   
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndexUpdated().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        
                        soHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getRreal(), currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        soHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getRreal(), currState.getEdge().getRreal(),joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getRreal(),currState.getEdge().getrCopied(), joinState.getEdge().getrCopied(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.soHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                
                if(Util.checker){
                    
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        soHashJoinRC2(currState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getrCopied(),currState.getEdge().getRreal(),joinState.getEdge().getRreal(), currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }
                    
                    
                    if(Util.checker){
                        
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        soHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(), currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        osHashJoinRC2(joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(), currState.getEdge().getRreal(),joinState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }
                    
                    
                    
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
        }
        
        //TODO: MOVE this shit somewhere or delete the fuck out it
        if(Util.checker){
            
            Util.checker=false;
             dd.setJoined(1);
             if(jdd!=null)
            jdd.setJoined(1);
             
            
            
        }else{
            Util.stop=1;
        }
    }
    
    ///////////////////////######################################################################////////////////////////////////////////
  
    @Override
    public void incrementalOSJoinStage(AutomataState currState, AutomataState joinState,Dependability dd,Dependability jdd){//AutomataState currState, AutomataState joinState, Automata a, long pred1, long pred2){
        
      
        
        
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
             
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                //  this.ssHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                 long index =Util.indexCount;
                if(currState.getRule().getC2().size()<joinState.getRule().getC2().size()){
                    
                    ///send it over here
                    
                    this.osHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),index,currState.getParentChild(),joinState.getParentChild());
                }else{
                    
                    
                    //send it over here
                    this.soHashJoinCC(joinState.getRule().getC2(),currState.getRule().getC2(),joinState.getEdge().getRreal(), currState.getEdge().getRreal(),index ,joinState.getParentChild(),currState.getParentChild());
                }
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    // this.osHashJoinRC(null, null, null, null);
                    
                    
                    
                    this.soHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    
                    
                    if(Util.checker){
                       
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    this.soHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    
                    
                    if(Util.checker){
                       
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.osHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                
                
                if(Util.checker){
                   
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndexUpdated().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        osHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2(joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }
                    
                    
                    
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getRreal().getR().size()){
                        osHashJoinRC2( currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2( joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }
                    
                    
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.osHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(),  currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                
                if(Util.checker){
                   
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    if(currState.getEdge().getRreal().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        osHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied() , currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2(joinState.getEdge().getrCopied() ,currState.getEdge().getRreal(), joinState.getEdge().getRreal(), currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }
                    //   osHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied() , currState.getEdge().getrCopied(),joinState.getEdge().getRreal());
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(currState.getEdge().getrCopied().getR().size()<joinState.getEdge().getrCopied().getR().size()){
                        osHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(), currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }else{
                        soHashJoinRC2(joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }
                    
                    
                    
                    
                    
                    if(Util.checker){
                       
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
        }
        if(Util.checker){
             dd.setJoined(1);
             if(jdd!=null)
            jdd.setJoined(1);
            Util.checker=false;
            
        }else{
            Util.stop=1;
        }
    }
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    
    
    
   
    @Override
    public void incrementalOOJoinStage(AutomataState currState, AutomataState joinState,Dependability dd,Dependability jdd){
        
      
        if(joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty()){ /// the use the both C and put it in Rreal of both
            
            ///first check if the r is empty or ecopied is emptied for the currstate
            
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                //  this.ssHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                  long index =Util.indexCount;
                if(currState.getRule().getC2().valueSize()<joinState.getRule().getC2().valueSize()){
                    
                    ///send it over here
                    
                    this.ooHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),index,currState.getParentChild(),joinState.getParentChild());
                }else{
                    
                    //send it over here
                    this.ooHashJoinCC(joinState.getRule().getC2(),currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),index,joinState.getParentChild(),currState.getParentChild());
                }
                
                // this.ooHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
                
                
                
            }else
                
                
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    // this.osHashJoinRC(null, null, null, null);
                    this.ooHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    if(Util.checker){
                        
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    this.ooHashJoinRC(joinState.getRule().getC2(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    
                    
                    if(Util.checker){
                        
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }
            
            
            //   this.ssHashJoinCC(currState.getRule().getC2(), joinState.getRule().getC2(), currState.getEdge().getRreal(), joinState.getEdge().getRreal());
            
            
            
            
        }else if(!joinState.getEdge().getRreal().getR().isEmpty() && joinState.getEdge().getrCopied().getR().isEmpty() ){
            //use Real
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.ooHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                
                
                if(Util.checker){
                    joinState.getEdge().getRreal().getR().clear();
                    joinState.getEdge().getRreal().getrIndexUpdated().clear();
                }
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    if(joinState.getEdge().getRreal().getR().valueSize()<currState.getEdge().getRreal().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getRreal(), currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        ooHashJoinRC2( currState.getEdge().getRreal(),joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    if(Util.checker){
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    if(joinState.getEdge().getRreal().getR().valueSize()<currState.getEdge().getrCopied().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getRreal(), currState.getEdge().getrCopied(), joinState.getEdge().getrCopied(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        ooHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getRreal(),currState.getEdge().getRreal(), joinState.getEdge().getrCopied(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    if(Util.checker){
                        joinState.getEdge().getRreal().getR().clear();
                        joinState.getEdge().getRreal().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                }
            
            
            
            
            // this.ssJoinOptimisedForRC(currState.getRule().getC2(), joinState.getEdge().getRreal(), joinState.getEdge().getrCopied(), currState.getEdge().getRreal());
            
            //    if(checker){
            //      joinState.getEdge().getRreal().getR().clear();
            //    joinState.getEdge().getRreal().getrIndex().clear();
            // }
            
        }else if(joinState.getEdge().getRreal().getR().isEmpty() && !joinState.getEdge().getrCopied().getR().isEmpty()){
            
            if(currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty()){
                this.ooHashJoinRC(currState.getRule().getC2(), joinState.getEdge().getrCopied(),  currState.getEdge().getRreal(),joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                
                if(Util.checker){
                    joinState.getEdge().getrCopied().getR().clear();
                    joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                    
                }
                
                
            }else
                
                if(!currState.getEdge().getRreal().getR().isEmpty() && currState.getEdge().getrCopied().getR().isEmpty() ){
                    
                    
                    
                    if(joinState.getEdge().getrCopied().getR().valueSize()<currState.getEdge().getRreal().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getrCopied(), currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getEdge().getrCopied(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        ooHashJoinRC2(currState.getEdge().getRreal(),joinState.getEdge().getrCopied(),currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    
                    if(Util.checker){
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getRreal().getR().clear();
                        currState.getEdge().getRreal().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                    
                }else if (currState.getEdge().getRreal().getR().isEmpty() && !currState.getEdge().getrCopied().getR().isEmpty()){
                    
                    
                    if(joinState.getEdge().getrCopied().getR().valueSize()<currState.getEdge().getrCopied().getR().valueSize()){
                        ooHashJoinRC2(joinState.getEdge().getrCopied(), currState.getEdge().getrCopied(), joinState.getEdge().getRreal(),currState.getEdge().getRreal(),joinState.getParentChild(),currState.getParentChild());
                    }else{
                        ooHashJoinRC2(currState.getEdge().getrCopied(),joinState.getEdge().getrCopied(),currState.getEdge().getRreal(), joinState.getEdge().getRreal(),currState.getParentChild(),joinState.getParentChild());
                    }
                    
                    
                    if(Util.checker){
                        joinState.getEdge().getrCopied().getR().clear();
                        joinState.getEdge().getrCopied().getrIndexUpdated().clear();
                        currState.getEdge().getrCopied().getR().clear();
                        currState.getEdge().getrCopied().getrIndexUpdated().clear();
                        
                        
                    }
                    
                    
                }
            
            
            
            
            
        }
        if(Util.checker){
             dd.setJoined(1);
             if(jdd!=null)
            jdd.setJoined(1);
            Util.checker=false;
            
        }else{
            Util.stop=1;
        }
        
        
    }
    
    
    
    
    
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    ///Modify it
  //ISSUE: 1
    @Override
    public void osHashJoinCC(MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2,long index, MultiBiMap<Indexes,Indexes> _pC,MultiBiMap<Indexes,Indexes> _jpC){
        
        for(Long o:_c1.keySet()){//change the order
            
            if(_c2.containsValue(o)){
                Util.checker=true;
              
                Util.parentChild.clear();
                //_r1.getR().putAll(o, _c1.get(o));
               
                Set<Long> val=_c1.get(o); ///why this
                for(long v:val){
                    
                    _r1.getR().put(o, v);
                    Indexes kin = new Indexes(++Util.perTableIndex,++index);
                    
                    _r1.getrIndexUpdated().put(kin,new SO(o, v));
                Util.parentChild.add(kin);
                }
             //   System.out.println("Values: "+ _c2.getKeys(index));
                ////////////////////
                Set<Long> keys =_c2.getKeys(o);  ///change it if its too expensive
                for(long k:keys){
                     _r2.getR().put(k, o);   //TODO: should I get all the values
                     
                 
                
                
                for(Indexes kin:Util.parentChild){
                    Indexes in= new Indexes(++Util.perTableIndex,kin.getGlobalIndex());
                    
                     _r2.getrIndexUpdated().put(in, new SO(k, o));
                     
//                     if(order ==1){
                        _pC.put(kin, in);
                        _jpC.put(in,kin);
//                     }else{
//                         _pC.put(in, kin);
//                        _jpC.put(kin,in);
//                     }
                }
                }
                
             //   _r2.getR().put(_c2.getKey(o), o);   //TODO: should I get all the values
               // _r2.getrIndex().put(index,new SO(_c2.getKey(o), o));  ///change it later
                
                ///get all the values of the with this key
                
            }
            
        }
        
        
        
   //   this.eachJoinResult(_r1, _r2,_pC,_jpC);
        
        
    }
    
    
   //Edit 4
    @Override
    public void osHashJoinRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
        
        
      //  if(_c1.keySize()<_r2.getR().valueSize()){
            
            for(long sub:_c1.keySet()){
                
                if(_r2.getR().containsValue(sub)){
              
                    Util.parentChild.clear();
                    Set<Long> keyall=_r2.getR().getKeys(sub);
                    
                    for(long ka:keyall){
                        _r3.getR().put(ka, sub);
                         SO sp= new SO(ka, sub);
   
                         
                         
                            Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(sp);
                         
                        for(Indexes kin:ind){
                            //indexes.add(kin.getGlobalIndex());
                            
                            Util.parentChild.add(kin);
                          _r3.getrIndexUpdated().put(kin, sp);   
                        }
                       
                    }
                    
                    Util.checker=true;
                    
                 //   SO sp= new SO(_r2.getR().getKey(sub), sub);
                 //   index=_r2.getrIndex().getKey(new SO(_r2.getR().getKey(sub), sub));
                    
                   ///get the both indexes from the _r2
                    
                    for(long val:_c1.get(sub)){  ///move it to the upper block
                        ///each of the value should be included in it with all the indexes
                        _r4.getR().put(sub, val);
                        
                        for(Indexes in:Util.parentChild){
                            
                       
                            
                            Indexes ind= new Indexes(++Util.perTableIndex,in.getGlobalIndex());
                            
                             _r4.getrIndexUpdated().put(ind, new SO(sub,val));  
                             
                            // if(order ==1){
                                 _pC.put(ind, in);
                             _jpC.put(in, ind);
//                             }else{
//                                 _pC.put(in, ind);
//                             _jpC.put(ind, in);
//                             }
                             
                            //_r4.getrIndex().put(in, new SO(sub,val));
                        }
                        
                        
                        
                        
                    }
                    
                    
                    
                    
                }
            }
                
           // }
//        }else{
//            //dd.clear();
//            
//            for(long sub:_r2.getR().values()){
//                if(_c1.containsKey(sub)){
//                        this.parentChild.clear();
//                    Set<Long> keyall=_r2.getR().getKeys(sub);
//                    
//                    for(long ka:keyall){
//                        _r3.getR().put(ka, sub);
//                         SO sp= new SO(ka, sub);
//                        // index=_r2.getrIndex().getKey(sp);
//                         
////                         Set<Long> in= _r2.getrIndex().getKeys(sp);
////                         for(long kin:in){
////                             this.indexes.add(kin);
////                         _r3.getrIndex().put(kin, sp);
////                         }
////                         
//                         
//                         
//                         
//                            Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(sp);
//                         
//                        for(Indexes kin:ind){
//                            //indexes.add(kin.getGlobalIndex());
//                            
//                            this.parentChild.add(kin);
//                          _r3.getrIndexUpdated().put(kin, sp);   
//                        }
//                       
//                    }
//                    
//                    checker=true;
//                    
//                 //   SO sp= new SO(_r2.getR().getKey(sub), sub);
//                 //   index=_r2.getrIndex().getKey(new SO(_r2.getR().getKey(sub), sub));
//                    
//                   ///get the both indexes from the _r2
//                    
//                    for(long val:_c1.get(sub)){
//                        ///each of the value should be included in it with all the indexes
//                        _r4.getR().put(sub, val);
//                        
//                        for(Indexes in:this.parentChild){
//                            
//                          //  dd.put(in, ++indexCount);
//                            // dd.put(indexCount,in);/// remove this later
//                            
//                            Indexes ind= new Indexes(++perTableIndex,in.getGlobalIndex());
//                            
//                             _r4.getrIndexUpdated().put(ind, new SO(sub,val));  
//                             _pC.put(in, ind);
//                             _jpC.put(ind, in);
//                            //_r4.getrIndex().put(in, new SO(sub,val));
//                        }
//                        
//                        
//                        
//                        
//                    }
//                    
//                    
//                }
//                
//            }
//            
      //  }
        
        
   this.eachJoinResult(_r4, _r3,_jpC,_pC);   
    // graphPairResult(dd);   
    }
    
    
    ///Edit 6, Sixth Join
    ///Issue 6:
    @Override
    public void osHashJoinRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex  _r4,MultiBidirectionalIndex _r3,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
      
        for(long sub:_c1.getR().keySet()){
            
            if(_r2.getR().containsValue(sub)){
               
                 Set<Long> keyall=  _r2.getR().getKeys(sub);
               
                     for(long ka:keyall){
                        
                        SO sp= new SO(ka,sub);
                        
                        
                        Set<Indexes> in=_r2.getrIndexUpdated().getKeys(sp);
                        
                        for(Indexes kin:in){
                              for(long val:_c1.getR().get(sub)){
                                  
                                  
                                  SO sp2= new SO(sub,val);
                                   Set<Indexes> in2=_c1.getrIndexUpdated().getKeys(sp2);
                                  for(Indexes rIndex:in2){
                                      if(rIndex.getGlobalIndex()==kin.getGlobalIndex()){
                                          _r3.getrIndexUpdated().put(kin, sp);
                                          _r3.getR().put(ka, sub);
                                          
                                          
                                          _r4.getrIndexUpdated().put(rIndex, sp2);
                                          _r4.getR().put(sub, val);
                                          
                                          _pC.put(rIndex, kin);
                                          _jpC.put(kin, rIndex);
                                          
//                                           _pC.put(rIndex, kin);
//                                          _jpC.put(kin, rIndex);
                                      }
                                  }
//////                                  
//                            if(_c1.getrIndexUpdated().getKey(new SO(sub,val)).getGlobalIndex()==kin.getGlobalIndex()){
//                                _r3.getrIndexUpdated().put(kin, sp);
//                                _r3.getR().put(ka, sub);
//                               // _pC.put(kin, _c1.getrIndexUpdated().getKey(new SO(sub,val)));
//                            }else{
//                            //    _pC.removeAll(kin);
//                               
//                            }
                             }
                        }
                        

                          
                    }
                
                
                ///get the first key and get how many keys other table have, if there are more than 1
                
                Util.checker=true;
    
//                for(long val:_c1.getR().get(sub)){
//               
//                      Set<Indexes> in=_c1.getrIndexUpdated().getKeys(new SO(sub,val));
//                    //Set<Long> in=_c1.getrIndex().getKeys(new SO(sub,val));
//                      for(Indexes kin:in){
//                          for(long k2:keyall){
//                           if(_r2.getrIndexUpdated().getKey(new SO(k2,sub)).getGlobalIndex()==kin.getGlobalIndex()){
//                             _r4.getrIndexUpdated().put(kin, new SO(sub,val));
//                                  _r4.getR().put(sub, val);
//                             //_jpC.put(kin, _r2.getrIndexUpdated().getKey(new SO(k2,sub)));
//                        }else{
//                          //   _jpC.removeAll(kin); ///dont think remove all works properly, don't use it, wastage of resource.
//                           }
//                        }
//                      }
////                   
//                }
                
                
//                 }else{
//                     
//                     
//                      for(long val:_c1.getR().get(sub)){
//                    _r4.getR().put(sub, val);
//                     SO sp= new SO(sub,val);
//                   Set<Long> in= _c1.getrIndex().getKeys(sp);
//                   
//                   for(long kin:in){
//                       this.indexes.add(kin);
//                       _r4.getrIndex().put(kin, sp);
//                   }
//                    
//                }
//                     
//                      
//                      
//                      
//                        checker=true;  
//                      
//                       for(long ka:keyall){
//                        _r3.getR().put(ka, sub);
//                        Set<Long> keyall2=_c1.getrIndex().getKeys(new SO(ka,sub));
//                       
//                        
//                        
//                        
//                        
//                        
//                        for(long kin: keyall2){
//                            for(long kin2:this.indexes){
//                                dd.put(kin2, kin);
//                            }
//                            _r3.getrIndex().put(kin, new SO(ka,sub));
//                        }
//                          
//                    }
//                      
//                      
//                      
//                     
//                 }
                 
                 
                 ////
                 
                 
                 
                 
                 
                 
                    
                
                
            }
            
        }
        this.eachJoinResult(_r4, _r3,_pC,_jpC);
        
      // graphPairResult(dd);
       
    }
    
    ///First Join
  
    @Override
    public void soHashJoinCC(MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2, long index, MultiBiMap<Indexes,Indexes> _pC,MultiBiMap<Indexes,Indexes> _jpC){
        
        for(Long o:_c1.values()){//change the order
            
            if(_c2.containsKey(o)){
                Util.checker=true;
            
                Util.parentChild.clear();
               // _r1.getR().put(_c1.getKey(o), o);
                
                Set<Long> keyall=_c1.getKeys(o);
          
                for(long  ka:keyall){
                     _r1.getR().put(ka, o);
                     
                                         Indexes kin= new Indexes(++Util.perTableIndex,++index);
                     
                      _r1.getrIndexUpdated().put(kin,new SO(ka, o));
                   Util.parentChild.add(kin);
                }
                
               
                
               
                
                Set<Long> val=_c2.get(o);
                for(long v:val){
                    _r2.getR().put(o,v);
                  
                
                for(Indexes kin:Util.parentChild){
                    Indexes in= new Indexes(++Util.perTableIndex,kin.getGlobalIndex());
                    _r2.getrIndexUpdated().put(in, new SO(o, v));
//                    if(order==1){   //Correction
//                        _pC.put(in, kin);
//                        _jpC.put(kin, in);
//                    }else{
                          _pC.put(kin, in);
                        _jpC.put(in, kin);
                   // }
                    
                }
                
                
                }
                ///get all the values of the with this key
                
            }
            
        }
        
        
        
       this.eachJoinResult(_r1, _r2,_pC,_jpC);
        
        
    }
    
   // Third Join
    ///Issue: 3
    @Override
    public void soHashJoinRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
        
        
       // if(_c1.size()<_r2.getR().size()){
         //_C1 is a join state
            for(long sub:_c1.values()){
                
                if(_r2.getR().containsKey(sub)){
                    //this.indexes.clear();
                   Util.parentChild.clear();
                    Util.checker=true;
                    ///first get the index value from r2
                  //  long index=_r2.getrIndex().getKey(new SO(sub,_r2.getR().getValue(sub)));
                    for(long val:_r2.getR().get(sub)){
                        _r4.getR().put(sub, val);
                        SO sp= new SO(sub,val);
//                        Set<Long> in=_r2.getrIndex().getKeys(sp);
//                        for(long kin:in){
//                            this.indexes.add(kin);
//                            _r4.getrIndex().put(kin, new SO(sub,val));
//                        }
//                        
                        
                         Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(sp);
                         
                        for(Indexes kin:ind){
                           // indexes.add(kin.getGlobalIndex());
                           Util.parentChild.add(kin);
                         _r4.getrIndexUpdated().put(kin, sp);  
                         
                         
                        }
                        
                        
                        
                    }
                    
                    
                    
                    
                  //  _r3.getR().put(_c1.getKey(sub), sub);
                    
                    Set<Long> keyall= _c1.getKeys(sub);
                    for(long ka:keyall){
                            _r3.getR().put(ka, sub);
                            for(Indexes kin:Util.parentChild){
                            // dd.put(kin, ++indexCount);
                              // dd.put( indexCount,kin);///remove this later
                               // _r3.getrIndex().put(kin, new SO(ka, sub));
                                Indexes in= new Indexes(++Util.perTableIndex,kin.getGlobalIndex());
                     _r3.getrIndexUpdated().put(in, new SO(ka, sub));  
//                     if(order==1){
//                          _pC.put(kin, in);
//                     _jpC.put(in, kin);
//                     }else{
                         _pC.put(in, kin);
                             _jpC.put(kin, in);
                    // }
                    
                            }
                            }
                    
                    
                    
                  
                    
                    
                    
                    
                }
                
            }
//        }else{
//            
//            for(long sub:_r2.getR().keySet()){
//                if(_c1.containsValue(sub)){
//                  //  this.indexes.clear();
//                    this.parentChild.clear();
//                    checker=true;
//                    ///first get the index value from r2
//                  //  long index=_r2.getrIndex().getKey(new SO(sub,_r2.getR().getValue(sub)));
//                    for(long val:_r2.getR().get(sub)){
//                        _r4.getR().put(sub, val);
//                        SO sp= new SO(sub,val);
////                        Set<Long> in=_r2.getrIndex().getKeys(sp);
////                        for(long kin:in){
////                            this.indexes.add(kin);
////                            _r4.getrIndex().put(kin, new SO(sub,val));
////                        }
////                        
//                        
//                         Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(sp);
//                         
//                        for(Indexes kin:ind){
//                           // indexes.add(kin.getGlobalIndex());
//                            this.parentChild.add(kin);
//                         _r4.getrIndexUpdated().put(kin, sp);  
//                         
//                         
//                        }
//                        
//                        
//                        
//                    }
//                    
//                    
//                    
//                    
//                  //  _r3.getR().put(_c1.getKey(sub), sub);
//                    
//                    Set<Long> keyall= _c1.getKeys(sub);
//                    for(long ka:keyall){
//                            _r3.getR().put(ka, sub);
//                            for(Indexes kin:this.parentChild){
//                            // dd.put(kin, ++indexCount);
//                              // dd.put( indexCount,kin);///remove this later
//                               // _r3.getrIndex().put(kin, new SO(ka, sub));
//                                Indexes in= new Indexes(++perTableIndex,kin.getGlobalIndex());
//                     _r3.getrIndexUpdated().put(in, new SO(ka, sub));  
//                     
//                     _pC.put(kin, in);
//                     _jpC.put(in, kin);
//                            }
//                            }
//                    
//                    
//                    
//                  
//                    
//                    
//                    
//                    
//                    
//                    
//                    
//                }
//            }
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//            
//        }
        
        
        
        this.eachJoinResult(_r3, _r4,_pC,_jpC);
       // this.graphPairResult(dd);
        
    }
    
    
    
   
    @Override
    public void soHashJoinRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4,MultiBiMap<Indexes,Indexes> _pC,MultiBiMap<Indexes,Indexes> _jpC){
        
        for(long sub:_c1.getR().values()){
            
            if(_r2.getR().containsKey(sub)){
                
                Util.checker=true;
               Set<Long> keyall=_c1.getR().getKeys(sub);
                Set<Long> valall=_r2.getR().get(sub);
                for(long val:valall){
                    
                    
                    
                    SO sp= new SO(sub, val);
                        
                        
                    Set<Indexes> in=_r2.getrIndexUpdated().getKeys(sp);
                    
                    for(Indexes kin:in){
                        for(long val2:keyall){
                            
                            SO sp2= new SO(val2,sub);
                            
                            Set<Indexes> in2=_c1.getrIndexUpdated().getKeys(sp2);
                            
                            
                            
                            
                            for (Indexes rIndex:in2) {
                                if(rIndex.getGlobalIndex()==kin.getGlobalIndex()){
                                    _r4.getR().put(sub, val);
                                    _r4.getrIndexUpdated().put(kin, sp);
                                    
                                    _r3.getR().put(val2, sub);
                                    _r3.getrIndexUpdated().put(rIndex, sp2);
                                    _pC.put(rIndex, kin);
                                    _jpC.put(kin, rIndex);
                                    
                                }
                            }
                            
//                                                    if(_c1.getrIndexUpdated().getKey(new SO(val2,sub)).getGlobalIndex()==kin.getGlobalIndex()){
//                                                           _r4.getR().put(sub, val);
//                                                           _r4.getrIndexUpdated().put(kin, sp);
//                                                            _pC.put(kin, _c1.getrIndexUpdated().getKey(new SO(val2,sub))); ///remove it
//                                                    }
                        }
                    }
                    
                    
                    
                   
                }
                
                
                
//                
//               
//                for(long ka:keyall){
//                    
//                        SO sp=new SO(ka, sub);
//                    Set<Indexes> in=_c1.getrIndexUpdated().getKeys(sp);
//                    for(Indexes kin:in){
//                          for(long k2:valall){
//                                                         if(_r2.getrIndexUpdated().getKey(new SO(sub,k2)).getGlobalIndex()==kin.getGlobalIndex()){
//                                                           _r3.getR().put(ka, sub);
//                                                           _r3.getrIndexUpdated().put(kin, sp);
//                                                            _jpC.put(kin, _r2.getrIndexUpdated().getKey(new SO(sub,k2))); ///remove it
//                                                         }
//                          }
//                    }
//                    
//                    
//                    
//                    
//                    
//                }
         
                
                
                
                
            }
            
        }
       // this.eachJoinResult(_r3, _r4);
    }
    
    
    ///////////////////////######################################################################////////////////////////////////////////
    
    
    
    
    
    
    
    
    
    /* public void joinFinalResults(MultiBiMap<SP, OP> c, MultiBiMap<SP, OP> finalSet, MultiBiMap<SP, OP> dummy, long pred_c, long pred_j){
    ///put the join result in the dummy and then put it back in the final ones
    for(SO sp:c.keySet()){
    if(finalSet.containsValue(new OP(sp.getSub(),pred_j))){
    dummy.put(sp, new OP(sp.getSub(),pred_c));
    }
    }
    
    finalSet.putAll(dummy);
    dummy.clear();
    
    }
    */
    
    
   
    @Override
    public void ooHashJoinCC(MultiBiMap<Long, Long> _c1,MultiBiMap<Long, Long> _c2,MultiBidirectionalIndex _r1,MultiBidirectionalIndex  _r2,long index, MultiBiMap<Indexes,Indexes> _pC, MultiBiMap<Indexes,Indexes> _jpC){
        
        
        for(Long obj:_c1.values()){
            if(_c2.containsValue(obj)){
                Util.parentChild.clear();
                Util.checker=true;
            
               Set<Long> keyall= _c1.getKeys(obj);
               for(long key:keyall){
                    _r1.getR().put(key, obj);
                    
                    Indexes kin =new Indexes(++Util.perTableIndex,++index);
                      _r1.getrIndexUpdated().put(kin, new SO(key, obj));
                    Util.parentChild.add(kin);
                    
               }
                
                
                
              
                Set<Long> keyall2= _c2.getKeys(obj);
                for(long key:keyall2){
                    _r2.getR().put(key, obj);
                  
                   
                   
                   
                
                
                for(Indexes kin:Util.parentChild){
                     Indexes in= new Indexes(++Util.perTableIndex,kin.getGlobalIndex());
                      _r2.getrIndexUpdated().put(in, new SO(key,obj));
                      
//                      if(order==1){
                          _pC.put(kin, in);
                          _jpC.put(in, kin);
//                      }else{
//                           _pC.put(in, kin);
//                          _jpC.put(kin, in);
//                      }
                      
                }
                }
                
                
            }
            
        }
        
       // this.eachJoinResult(_r1, _r2);
    }
    
    
   
    @Override
    public void ooHashJoinRC(MultiBiMap<Long, Long> _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4,MultiBiMap<Indexes,Indexes> _pC,MultiBiMap<Indexes,Indexes> _jpC){
        
        
        
       // if(_c1.valueSize()<_r2.getR().valueSize()){
            
            
            for(long obj:_c1.values()){
                if(_r2.getR().containsValue(obj)){
                    Util.parentChild.clear();
                    Util.checker=true;
                    ///first get the index from the _r2
                  //  long index= _r2.getrIndex().getKey(new SO(_r2.getR().getKey(obj),obj));
                    
                  ////so r2 contains the keys, get the index from r2 and put it in the c ones
                    
                     Set<Long> keyall2=_r2.getR().getKeys(obj);
                     for(long keys:keyall2 ){
                      _r4.getR().put(keys,obj);
                      
                       SO sp= new SO(keys, obj);
                         Set<Indexes> ind=_r2.getrIndexUpdated().getKeys(sp);
                         
                          for(Indexes kin:ind){
                                Util.parentChild.add(kin);
                                _r4.getrIndexUpdated().put(kin, sp);
                          }
                     }
                    
                    
                    Set<Long> keyall= _c1.getKeys(obj);
                    
                    
                    for(long keys:keyall){
                         _r3.getR().put(keys, obj);
                         SO sp= new SO(keys, obj);
                        
                         
                         
                       for(Indexes kin:Util.parentChild){
                          Indexes in= new Indexes(++Util.perTableIndex,kin.getGlobalIndex());
                          _r3.getrIndexUpdated().put(in, sp);
                          
                              _pC.put(in, kin);
                          _jpC.put(kin, in);
                          
                          
                      }

           
                         
                    }
                    
                   
                    
                   
//                  for(long keys:keyall2 ){
//                      _r4.getR().put(keys,obj);
//                      
//                      
//                      
//                      for(Indexes kin:this.parentChild){
//                          Indexes in= new Indexes(++perTableIndex,kin.getGlobalIndex());
//                          _r4.getrIndexUpdated().put(kin, new SO(keys,obj));
//                          if(order==1){
//                              _pC.put(kin, in);
//                          _jpC.put(in, kin);
//                          }else{
//                              _pC.put(in, kin);
//                          _jpC.put(kin, in);
//                          }
//                          
//                      }
//                      
//                  }
//                    
//                    
                    
                   
                    
                    
                    
                    
                    
                    
                    
                }
                
                
            }
            
            
            
//        }else{
//            
//            
//            for(long obj:_r2.getR().values()){
//                if(_c1.containsValue(obj)){
//                    this.indexes.clear();
//                    checker=true;
//                  
//                    Set<Long> keyall= _c1.getKeys(obj);
//                    
//                    
//                    for(long keys:keyall){
//                         _r3.getR().put(keys, obj);
//                         SO sp= new SO(keys, obj);
//                         
//                         Set<Long> in=_r2.getrIndex().getKeys(sp);
//                         for(long kin:in){
//                             this.indexes.add(kin);
//                               _r3.getrIndex().put(kin, sp);
//                         }
//                    }
//                    
//                   
//                    
//                    Set<Long> keyall2=_r2.getR().getKeys(obj);
//                  for(long keys:keyall2 ){
//                      _r4.getR().put(keys,obj);
//                      
//                      for(long kin:this.indexes)
//                           _r4.getrIndex().put(kin, new SO(_r2.getR().getKey(obj),obj));
//                  }
//                    
//                    
//                    
//                    
//                }
//                
//                
//            }
//            
//            
//        }
        
        
     // this.eachJoinResult(_r3, _r4);  
    }
    
    
    
    
   
    @Override
    public void ooHashJoinRC2(MultiBidirectionalIndex _c1,MultiBidirectionalIndex _r2,MultiBidirectionalIndex _r3,MultiBidirectionalIndex  _r4,MultiBiMap<Indexes,Indexes> _pC,MultiBiMap<Indexes,Indexes> _jpC){
        
        
        for(long obj:_c1.getR().values()){
            if(_r2.getR().containsValue(obj)){
               
                
                
                ///get the keys from c1
                
                Set<Long> keyall=_c1.getR().getKeys(obj);  
                 Set<Long> keyall2= _r2.getR().getKeys(obj);
                
                for(long keys:keyall){ //coming from c
                    
                    
                    SO sp= new SO(keys, obj);
                        
                        
                        Set<Indexes> in=_c1.getrIndexUpdated().getKeys(sp);
                        
                        for(Indexes kin:in){
                             for(long val:keyall2){
                                 
                                SO sp2= new SO(val,obj) ;
                                
                                Set<Indexes> in2=_r2.getrIndexUpdated().getKeys(sp2);
                                
                                for(Indexes rIndex:in2 ){
                                    if(rIndex.getGlobalIndex()==kin.getGlobalIndex()){
                                      _r3.getR().put(keys, obj);
                                                                  
                                       _r3.getrIndexUpdated().put(kin, sp);  
                                       
                                       
                                        _r4.getR().put(val,obj);
                                _r4.getrIndexUpdated().put(rIndex, sp2);
                                
                                _jpC.put(rIndex, kin);
                                _pC.put(kin, rIndex);
                                       
                                    }
                                }
                                 
//                             if(_r2.getrIndexUpdated().getKey(new SO(val,obj)).getGlobalIndex()==kin.getGlobalIndex()){
//                                                                  _r3.getR().put(keys, obj);
//                                                                  
//                                                                    _r3.getrIndexUpdated().put(kin, sp);
//                                                            } 
                             }
                        }
                    
                    
                    
                    
                    
                   
                    
                }
                        
                Util.checker=true;
                ///first get the index from the _r2
             
//                
//                for(long keys:keyall2){
//                    SO sp= new SO(keys,obj);
//                    Set<Indexes> in=_r2.getrIndexUpdated().getKeys(sp);
//                    for(Indexes kin:in){
//                        for(long k2:keyall){
//                            if(_c1.getrIndexUpdated().getKey(new SO(k2,obj)).getGlobalIndex()==kin.getGlobalIndex()){
//                                _r4.getR().put(keys,obj);
//                                _r4.getrIndexUpdated().put(kin, sp);
//                            }
//                        }
//                    }
//                    
//                    
//                }
//                
                
                
                
                
                
                
            }
            
            
        }
        
        
      // this.eachJoinResult(_r3, _r4);
        
    }
    
    
    @Override
    public void eachJoinResult(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _c2,MultiBiMap <Indexes,Indexes> _pC, MultiBiMap <Indexes,Indexes> _jpC){
       
//      System.out.println("Map 1");
//        
//        for (Map.Entry<Indexes, SO> entry : _c1.getrIndexUpdated().entries())
//                 {
//    //System.out.println(entry.getKey().toString() + "-->" + entry.getValue());
//       System.out.println(entry.getKey().toString()+"--> Subject  "+ ((SO)entry.getValue()).getSub() +"--> Object " +  ((SO)entry.getValue()).getPred());
//                 }
//        
//        for(Entry<Indexes, Indexes> entry:_pC.entries()){
//              System.out.println("Parent "+entry.getKey()+" Child "+ entry.getValue());
//        }
//        
//        System.out.println("Map 2");
//        
//        for (Map.Entry<Indexes, SO> entry : _c2.getrIndexUpdated().entries())
//                 {
//  //  System.out.println(entry.getKey().toString() + "-->" + entry.getValue());
//     System.out.println(entry.getKey().toString()+"--> Subject  "+ ((SO)entry.getValue()).getSub() +"--> Object " +  ((SO)entry.getValue()).getPred());
//                 }
//         for(Entry<Indexes, Indexes> entry:_jpC.entries()){
//              System.out.println("Parent "+entry.getKey()+" Child "+ entry.getValue());
//        }
//        
//       
//        System.out.println("#####################################");
//      
    }
    
    
    
    @Override
    public void graphPairResult(Multimap<Long,Long> m){
         System.out.println("Iteration");
        for (long key : m.keySet()) { 
           
            
            System.out.println("From " + key +" To "+m.get(key));
        }
          System.out.println("#####################################");
    }
    
    
    
    

}
