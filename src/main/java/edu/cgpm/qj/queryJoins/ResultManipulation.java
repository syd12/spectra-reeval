/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.queryJoins;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Sets;
import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.SO;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import org.semanticweb.yars.nx.BNode;
import org.semanticweb.yars.nx.Literal;
import org.semanticweb.yars.nx.Resource;

/**
 *
 * @author sydgillani
 */
public class ResultManipulation {
    
      Multimap<SO,SO> graph;

    public ResultManipulation() {
        graph = HashMultimap.create();
    }
    
    protected BlockingQueue<String> resultqueue;

    public void setResultqueue(BlockingQueue<String> resultqueue) {
        this.resultqueue = resultqueue;
    }
    
    
     public void indexExtraction_Case1(List<AutomataState> a, FinalView[] _FV, DictionaryOpImpl dic, long timestamp, int stateID) {
        Set<Long> keys = null;

        List<Set<SO>> setList = new ArrayList<>();
        // this.stateID=5;
        AutomataState keyState = a.stream().filter(x -> x.getStateId() == stateID).findFirst().orElse(null);

        if (keyState == null) {
            System.out.println("");
        } else {

            if (!keyState.getEdge().getRreal().getR().isEmpty()) {

                keys = keyState.getEdge().getRreal().getrIndex().keySet();
                
               

            } else if (!keyState.getEdge().getrCopied().getR().isEmpty()) {

                keys = keyState.getEdge().getrCopied().getrIndex().keySet();
            }

            int seq = 1;
            for (long in : keys) {
                // statesDFS(a.getStates().get(stateID), a, in);
                resultExtraction_Case1(in,in, a, setList, _FV, timestamp, seq);
                seq++;
               //   printOut(setList, dic);
                ///Print out the results only if this index exists in all the views in FV
                
                if(indexChecker(in,_FV)){
                     printOut(setList, dic);
                }
                
               
            }

        }
    }
     public boolean indexChecker(long index, FinalView[] _FV){
       for(FinalView fv:_FV){
           if(!fv.getrIndex().containsKey(index)){
               return false;
           }
       }
    
    return true;
    }     
        public void resultExtraction_Case1(long index, long pIndex, List<AutomataState> a, List<Set<SO>> setList, FinalView[] _FV, long timestamp, int seq) {
        Set<SO> nSP;

        setList.clear();
        for (AutomataState s : a) {
            nSP = new HashSet<>();
            if (!s.getEdge().getRreal().getrIndex().isEmpty()) {

             
               
                     setList.add(extractioFromrIndex(s, s.getEdge().getRreal().getrIndex(), index,pIndex, _FV[s.getStateId()], timestamp, nSP));
               
                ///setlist to output the results

            } else {

              
                //   _FV[s.getStateId()].getTimeIndexMap().put(timestamp, index);
                setList.add(extractioFromrIndex(s, s.getEdge().getrCopied().getrIndex(), index,pIndex, _FV[s.getStateId()], timestamp, nSP));
            }

            s.setChange(0);
//            if (seq == 1) {
//                _FV[s.getStateId()].getTimeList().add(timestamp);
//            }
        }

    }
    
    
    
    
    public Set<SO> extractioFromrIndex(AutomataState s, MultiBiMap<Long, SO> table,long index, long pIndex, FinalView _FV, long timestamp, Set<SO> nSP){
           for (Iterator<Map.Entry<Long, SO>> it = table.entries().iterator(); it.hasNext();) {
                    Map.Entry<Long, SO> sp = it.next();
                    if (sp.getKey() == index) {
                        _FV.getR().put(sp.getValue().getSub(), sp.getValue().getPred());
                        _FV.getTimeSPpairs().put(timestamp, sp.getValue()); ////also the time tree will be effective over here
                        _FV.getrIndex().put(pIndex, sp.getValue());

                        s.getRule().getC2().remove(sp.getValue().getSub(), sp.getValue().getPred()); ///remove the timestamp from the 
                       
                         s.getRule().getTimeSOpairs().remove(timestamp, new SO(sp.getValue().getSub(),sp.getValue().getPred()));
                        
                        nSP.add(sp.getValue());///add to FV in the required table
                    }
                }
           
           return nSP;
    }
     public void printOut(List<Set<SO>> setList, DictionaryOpImpl dic) {

        Set<List<SO>> result = Sets.cartesianProduct(setList);
        result.forEach((outList) -> {

            outList.forEach((out) -> {
               // System.out.print("--> Subject  " + dic.getResourceIncremenal(out.getSub()) + "--> Object " + dic.getResourceIncremenal(out.getPred()));
                
              //  String re= "--> Subject  "+ dic.getResourceIncremenal(out.getSub()) + "--> Object " + dic.getResourceIncremenal(out.getPred());
              String re="";
                Resource s = dic.getResourceIncremenal(out.getSub());
                
                if(s == null){
                   BNode b= dic.getBnodeIncremental(out.getSub());
                   
                   re= "Subject -->  "+b.toString();
                }else{
                    re= "Subject --> "+s.toString();
                }
                    
                 Resource o = dic.getResourceIncremenal(out.getPred());
                
                 if(o==null){
                     ///then literal//else bNode
                      Literal l = dic.getLiteralIncremental(out.getPred());
                      if(l==null){
                           BNode bo=dic.getBnodeIncremental(out.getPred());
                           
                           re = re+ "       Object -->  "+bo.toString();
                      }else{
                          re =  re+ "        Object -->  "+l.getValue();
                      }
                 }else{
                     re =  re + "        Object -->  "+o.toString();
                 }
                 
                 
                System.out.println(re);
             
               
                  //  s.getRule().getC2().remove(out.getSub(), out.getPred());
            });
             //resultqueue.add("######################################");
           System.out.println("######################################");

        });
//result.clear();
    }      
  ////////////////////////////////////////////////////////////////////////////////////////
     
     /////////////////////////Case 2 Stuff/////////////////////////////////////////////

   
public void indexExtraction_Case2(List<AutomataState> a, FinalView[] _FV, DictionaryOpImpl dic, long timestamp, int stateID,Utilities Util) {
        Collection<Map.Entry<Long,Long>> parentKeys = null;
       Set<Indexes> keys = null;
       // List<Set<SO>> setList = new ArrayList<>();
        // this.stateID=5;
         Util.setList.clear();
        AutomataState keyState = a.stream().filter(x -> x.getStateId() == stateID).findFirst().orElse(null);

        if (keyState == null) {
            System.out.println("");
        } else {

            if (!keyState.getEdge().getRreal().getR().isEmpty()) {

            keys=keyState.getEdge().getRreal().getrIndexUpdated().keySet();
                
                parentKeys= keyState.getEdge().getRreal().getParentChild().entries();

            } else if (!keyState.getEdge().getrCopied().getR().isEmpty()) {

                 parentKeys= keyState.getEdge().getrCopied().getParentChild().entries();
                  keys=keyState.getEdge().getrCopied().getrIndexUpdated().keySet();
            }

            int seq = 1;
            for (Map.Entry<Long,Long> pIndex : parentKeys) {
                
               /////check the parent child relationship and then extract the result with the indexes
                
                if(case2Checker(a, pIndex.getKey(), pIndex.getValue())){
                     for(Indexes in:keys){
                   if(in.getGlobalIndex() == pIndex.getValue()){
                       ////get this index and extract the matches
                       
                       this.resultExtraction_Case2(in, a, _FV, timestamp,Util);
                      
                       
                       this.printOut(Util.setList, dic);   ///in this case also check, if the this set of triples are avaiable in all of them
                   
                   Util.setList.clear();
                   }
               }
                seq++;
                }
           
                
            }
            
            
            

        }
    }
      
        public boolean case2Checker(List<AutomataState> a, long pIndex,long cIndex){
   for(AutomataState s: a){
       if(!s.getEdge().getRreal().getR().isEmpty()){
          if(!s.getEdge().getRreal().getParentChild().isEmpty() && !s.getEdge().getRreal().getParentChild().containsEntry(pIndex, cIndex)){
           return false;
       }
       }else{
            if(!s.getEdge().getrCopied().getParentChild().isEmpty() && !s.getEdge().getrCopied().getParentChild().containsEntry(pIndex, cIndex)){
           return false;
       }
       }
   }
   
   return true;
  }  
  
        
        
        
  public void resultExtraction_Case2( Indexes index,List<AutomataState> a,FinalView[] _FV, long timestamp, Utilities Util){
         ////go throught the list and get the pairs 
         
        // Map<Integer, Set<SO>> closet= new HashMap<>();
         Util.closet.clear();
           graph.clear();
         for(int i=Util._joinOrder.size()-1;i>=0;i=i-2){
             ///get the first and then next of first one
            // System.out.println(Util._joinOrder.get(i-1) +" ," +Util._joinOrder.get(i));
             if(Util.closet.containsKey(Util._joinOrder.get(i-1))  && Util.closet.containsKey(Util._joinOrder.get(i)) ){
                 //skip it, or continue;
                 
                 continue; ///unnessary 
             }else if(Util.closet.containsKey(Util._joinOrder.get(i-1))){
                
                  Set<SO> set=extractResults( getTheState(a,Util._joinOrder.get(i-1)) , getTheState(a,Util._joinOrder.get(i)), index, Util.closet.get(Util._joinOrder.get(i-1)),_FV, timestamp, Util);
                  Util.closet.put(Util._joinOrder.get(i),set);
              //    System.out.println(set);
                 // a.get(Util._joinOrder.get(i-1)).setChange(0);
                 // a.get(Util._joinOrder.get(i)).setChange(0);
                 //   Util.setList.add(set);
                 
                 ///send this i and get the set from the closet
             }else if(Util.closet.containsKey(Util._joinOrder.get(i))){
                 
                   Set<SO> set= extractResults(getTheState(a,Util._joinOrder.get(i)), getTheState(a,Util._joinOrder.get(i-1)), index, Util.closet.get(Util._joinOrder.get(i)),_FV, timestamp, Util);
                   Util.closet.put(Util._joinOrder.get(i-1),set);
                //     System.out.println(set);
                    // a.get(Util._joinOrder.get(i-1)).setChange(0);
                 // a.get(Util._joinOrder.get(i)).setChange(0);
                  //  Util.setList.add(set);
                 ///send i+1 and the set got from the closet
             }else{
                 ///send a new set with i and i+1 as second
                 
                 Set<SO> set = new HashSet<>();
       
                 Set<SO> set2=extractResults(getTheState(a,Util._joinOrder.get(i-1)), getTheState(a,Util._joinOrder.get(i)), index, set, _FV, timestamp , Util);
            Util.closet.put(Util._joinOrder.get(i),set2);
            Util.closet.put(Util._joinOrder.get(i-1), set);
       //a.get(Util._joinOrder.get(i-1)).setChange(0);
              //    a.get(Util._joinOrder.get(i)).setChange(0);
        //    System.out.println(set);
          //Util.setList.add(set2);
             }
         }
     }
            
        
  
  public AutomataState getTheState(List<AutomataState> a, int id){
      
     for(AutomataState s:a){
         if(s.getStateId()==id){
             
             s.setChange(0);
             return s;
         }
                 
     }
      return null;
  }
        
 public Set<SO> extractResults(AutomataState s, AutomataState js, Indexes index, Set<SO> set, FinalView[] _FV, long timestamp, Utilities Util){
   Set<SO> newSet = new HashSet<>();
        if(set.isEmpty()){
            
            
            
            ///get it from S and 
            
            if(!s.getEdge().getRreal().getR().isEmpty()){
                
                
            
                         
                         this.firstStage(s.getEdge().getRreal().getrIndexUpdated(), index, s, _FV, timestamp, set, Util);
                 // setList.add(set);
              //    System.out.println(set);
                secondStage(s, js, newSet, set,_FV,timestamp);
                
                
                
            }else if(!s.getEdge().getrCopied().getR().isEmpty()){
                
                    
                        
                        firstStage(s.getEdge().getrCopied().getrIndexUpdated(), index, s, _FV, timestamp, set, Util);
                         //   System.out.println(set);
                         secondStage(s, js, newSet, set,_FV,timestamp);
                   // return set;
                    
               
            }
           
     }else{
         ///go around the set and get the childeren from the results set and then get the values from join state
         
          secondStage(s, js, newSet, set,_FV,timestamp);
     }
        
      return newSet;  
    }
    
    
    public void firstStage(MultiBiMap result, Indexes index, AutomataState s, FinalView[] _FV, long timestamp,Set<SO> set, Utilities Util){
        
                         for (Iterator<Map.Entry<Indexes, SO>> it = result.entries().iterator(); it.hasNext();) {
                             Map.Entry<Indexes,SO> sp = it.next();
                             if(sp.getKey().getGlobalIndex()==index.getGlobalIndex()){
                                 
                              //   System.out.println("State for FV: "+s.getStateId());
                                 _FV[s.getStateId()].getR().put(sp.getValue().getSub(), sp.getValue().getPred());
                               //  _FV[s.getStateId()].getTimeSPpairs().put(timestamp, sp.getValue());   ///temporal aspects like time tree should be captured over here
                               //  System.out.println("subject "+ sp.getValue().getSub() +"  Object "+ sp.getValue().getPred());
                                   _FV[s.getStateId()].getrIndex().put(index.getGlobalIndex(), sp.getValue());
                                     s.getRule().getC2().remove(sp.getValue().getSub(), sp.getValue().getPred());
                                set.add(sp.getValue());
                             }
                         }
                         
                        
                         
       
                         
                         
                         
                      //   Util.setList.add(set);
    }
    
     public void secondStage(AutomataState s, AutomataState js,Set<SO> newSet, Set<SO> set, FinalView[] _FV, long timestamp){
         
        // Set<SP> toBeRemoved= new HashSet<>();
         for(SO sp: set){
             /////get the parent from s
             
             
             Set<Indexes> kin=null;
             if(!s.getEdge().getRreal().getR().isEmpty()){
                 kin = s.getEdge().getRreal().getrIndexUpdated().getKeys(sp);
             }else if (!s.getEdge().getrCopied().getR().isEmpty()){
                 kin = s.getEdge().getrCopied().getrIndexUpdated().getKeys(sp);
             }
             
             
             for(Indexes k :kin){
                 ///get from js
                 //get the parent from
                 
                 
                 
                 for(Indexes d:  s.getParentChild().get(k)){
                     
                     if(!js.getEdge().getRreal().getR().isEmpty()){
                        // newSet.addAll(js.getEdge().getRreal().getrIndexUpdated().get(d)); ///if not then remove it
                         ///add the FV stuff over here
                         
                         for(SO so:js.getEdge().getRreal().getrIndexUpdated().get(d)){
                             // System.out.println("State for FV: "+js.getStateId());
                              _FV[js.getStateId()].getR().put(so.getSub(), so.getPred());
                              //   _FV[js.getStateId()].getTimeSPpairs().put(timestamp, so);   ///temporal aspects like time tree should be captured over here
                                graph.put(sp, so);
                                   _FV[js.getStateId()].getrIndex().put(d.getGlobalIndex(), so);
                                   
                                     js.getRule().getC2().remove(so.getSub(), so.getPred());
                             
                             newSet.add(so);
                         }
                         
                         
                         ///from the other set as well
                     }else if (!js.getEdge().getrCopied().getR().isEmpty()){
                         
                         if(!js.getEdge().getrCopied().getrIndexUpdated().get(d).isEmpty()){
                              for(SO so:js.getEdge().getrCopied().getrIndexUpdated().get(d)){
                                   _FV[js.getStateId()].getR().put(so.getSub(), so.getPred());
                               //  _FV[js.getStateId()].getTimeSPpairs().put(timestamp, so);   ///temporal aspects like time tree should be captured over here
                               
                                   _FV[js.getStateId()].getrIndex().put(d.getGlobalIndex(), so);
                                     js.getRule().getC2().remove(so.getSub(), so.getPred());
                                      graph.put(sp, so);
                                      newSet.add(so);
                              }
                         }
                         
                         
                         
                     }
                     
                 }
                 
             }
             
             
             
         }
        /// set.removeAll(toBeRemoved);
        
     }       
     
        
    public void start(Set<SO> visited,DictionaryOpImpl dic){
       visited.clear();
       
        for(SO so:graph.keySet()){
            if(!visited.contains(so)){
            //     visited = new HashSet<>();
            DFS(graph,so,visited,dic);
           // System.out.println("##################");
            }
            
            
        }
        System.out.println("######################################");
        
    }
    public void DFS(Multimap<SO,SO> g, SO so, Set<SO> visited,DictionaryOpImpl dic){
       visited.add(so);
       
        printing(so,dic);
       
      
       for(SO edge:g.get(so)){
           if(!visited.contains(edge)){
               ///printout
                
               DFS(g,edge,visited,dic);
           }
       }
        
    }
    
    
    public void printing(SO out,DictionaryOpImpl dic){
        String re="";
                Resource s = dic.getResourceIncremenal(out.getSub());
                
                if(s == null){
                   BNode b= dic.getBnodeIncremental(out.getSub());
                   
                   re= "Subject -->  "+b.toString();
                }else{
                    re= "Subject --> "+s.toString();
                }
                    
                 Resource o = dic.getResourceIncremenal(out.getPred());
                
                 if(o==null){
                     ///then literal//else bNode
                      Literal l = dic.getLiteralIncremental(out.getPred());
                      if(l==null){
                           BNode bo=dic.getBnodeIncremental(out.getPred());
                           
                           re = re+ "       Object -->  "+bo.toString();
                      }else{
                          re =  re+ "        Object -->  "+l.getValue();
                      }
                 }else{
                     re =  re + "        Object -->  "+o.toString();
                 }
                 
                 
                System.out.println(re);
    }
     
    ////////////////////////For EventBased Evalutiona///////////////////////
     
     
      public void resultExtraction_Event( Indexes index,List<AutomataState> a, Utilities Util){
         ////go throught the list and get the pairs 
         
        // Map<Integer, Set<SO>> closet= new HashMap<>();
               Util.closet.clear();
         
         graph.clear();

         for(int i=Util._joinOrder.size()-1;i>=0;i=i-2){
             ///get the first and then next of first one
          //   System.out.println(Util._joinOrder.get(i-1) +" ," +Util._joinOrder.get(i));
             if(Util.closet.containsKey(Util._joinOrder.get(i-1))  && Util.closet.containsKey(Util._joinOrder.get(i)) ){
                 //skip it, or continue;
                 
                 continue; ///unnessary 
             }else if(Util.closet.containsKey(Util._joinOrder.get(i-1))){
                
                  Set<SO> set=extractResultsEvent( getTheState(a,Util._joinOrder.get(i-1)) , getTheState(a,Util._joinOrder.get(i)), index, Util.closet.get(Util._joinOrder.get(i-1)), Util);
                  Util.closet.put(Util._joinOrder.get(i),set);
                 // System.out.println(set);
                 // a.get(Util._joinOrder.get(i-1)).setChange(0);
                 // a.get(Util._joinOrder.get(i)).setChange(0);
                   // Util.setList.add(set);
                 
                 ///send this i and get the set from the closet
             }else if(Util.closet.containsKey(Util._joinOrder.get(i))){
                 
                   Set<SO> set= extractResultsEvent(getTheState(a,Util._joinOrder.get(i)), getTheState(a,Util._joinOrder.get(i-1)), index, Util.closet.get(Util._joinOrder.get(i)), Util);
                   Util.closet.put(Util._joinOrder.get(i-1),set);
                   //  System.out.println(set);
                    // a.get(Util._joinOrder.get(i-1)).setChange(0);
                 // a.get(Util._joinOrder.get(i)).setChange(0);
                  //  Util.setList.add(set);
                 ///send i+1 and the set got from the closet
             }else{
                 ///send a new set with i and i+1 as second
                 
                 Set<SO> set = new HashSet<>();
       
                 Set<SO> set2=extractResultsEvent(getTheState(a,Util._joinOrder.get(i-1)), getTheState(a,Util._joinOrder.get(i)), index, set , Util);
            Util.closet.put(Util._joinOrder.get(i),set2);
            Util.closet.put(Util._joinOrder.get(i-1), set);
       //a.get(Util._joinOrder.get(i-1)).setChange(0);
              //    a.get(Util._joinOrder.get(i)).setChange(0);
         //   System.out.println(set);
          //Util.setList.add(set2);
             }
         }
     }
     
             
 public Set<SO> extractResultsEvent(AutomataState s, AutomataState js, Indexes index, Set<SO> set, Utilities Util){
   Set<SO> newSet = new HashSet<>();
        if(set.isEmpty()){
            
            
            
            ///get it from S and 
            
            if(!s.getEdge().getRreal().getR().isEmpty()){
                
                
            
                         
                         this.firstStageEvent(s.getEdge().getRreal().getrIndexUpdated(), index, s, set, Util);
                 // setList.add(set);
               //   System.out.println(set);
                secondStageEvent(s, js, newSet, set);
                
                
                
            }else if(!s.getEdge().getrCopied().getR().isEmpty()){
                
                    
                        
                        firstStageEvent(s.getEdge().getrCopied().getrIndexUpdated(), index, s, set, Util);
                           // System.out.println(set);
                         secondStageEvent(s, js, newSet, set);
                   // return set;
                    
               
            }
           
     }else{
         ///go around the set and get the childeren from the results set and then get the values from join state
         
          secondStageEvent(s, js, newSet, set);
     }
        
      return newSet;  
    }
  
   
     
      public void firstStageEvent(MultiBiMap result, Indexes index, AutomataState s,Set<SO> set, Utilities Util){
        
                         for (Iterator<Map.Entry<Indexes, SO>> it = result.entries().iterator(); it.hasNext();) {
                             Map.Entry<Indexes,SO> sp = it.next();
                             if(sp.getKey().getGlobalIndex()==index.getGlobalIndex()){
                                 
                            //     System.out.println("State for FV: "+s.getStateId());
                                 
                                     s.getRule().getC2().remove(sp.getValue().getSub(), sp.getValue().getPred());
                                        set.add(sp.getValue());
                             }
                         }
                         
                        // Util.setList.add(set);
    }
    
     public void secondStageEvent(AutomataState s, AutomataState js,Set<SO> newSet, Set<SO> set){
         
        // Set<SP> toBeRemoved= new HashSet<>();
         for(SO sp: set){
             /////get the parent from s
             
             
             Set<Indexes> kin=null;
             if(!s.getEdge().getRreal().getR().isEmpty()){
                 kin = s.getEdge().getRreal().getrIndexUpdated().getKeys(sp);
             }else if (!s.getEdge().getrCopied().getR().isEmpty()){
                 kin = s.getEdge().getrCopied().getrIndexUpdated().getKeys(sp);
             }
             
             
             for(Indexes k :kin){
                 ///get from js
                 //get the parent from
                 
                 
                 
                 for(Indexes d:  s.getParentChild().get(k)){
                     
                     if(!js.getEdge().getRreal().getR().isEmpty()){
                        // newSet.addAll(js.getEdge().getRreal().getrIndexUpdated().get(d)); ///if not then remove it
                         ///add the FV stuff over here
                         
                         for(SO so:js.getEdge().getRreal().getrIndexUpdated().get(d)){
                             // System.out.println("State for FV: "+js.getStateId());
                             
                                     js.getRule().getC2().remove(so.getSub(), so.getPred());
                             graph.put(sp, so);
                             newSet.add(so);
                         }
                         
                         
                         ///from the other set as well
                     }else if (!js.getEdge().getrCopied().getR().isEmpty()){
                         
                         if(!js.getEdge().getrCopied().getrIndexUpdated().get(d).isEmpty()){
                              for(SO so:js.getEdge().getrCopied().getrIndexUpdated().get(d)){
                                
                                     js.getRule().getC2().remove(so.getSub(), so.getPred());
                                     graph.put(sp, so);
                                      newSet.add(so);
                              }
                         }
                         
                         
                         
                     }
                     
                 }
                 
             }
             
             
             
         }
        /// set.removeAll(toBeRemoved);
        
     }       
       /////////////////////////////////////////////////////////////////////
     
}
