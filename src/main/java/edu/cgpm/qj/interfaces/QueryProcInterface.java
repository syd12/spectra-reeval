/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.interfaces;

import com.google.common.collect.Multimap;
import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.MultiBidirectionalIndex;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.rulesmodel.Dependability;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author sydgillani
 */
public interface QueryProcInterface {

    void _resultExtraction(long index, List<AutomataState> a, FinalView[] _FV, DictionaryOpImpl dic, long timestamp, int seq);

    void eachJoinResult(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _c2, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void generalStage(int size, List<AutomataState> aStates, DictionaryOpImpl dic, FinalView[] _FV, long timestamp, long range, long step);

    void graphPairResult(Multimap<Long, Long> m);

    void incrementalOOJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);

    ///////////////////////######################################################################////////////////////////////////////////
    void incrementalOSJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);
    ///////////////////////######################################################################////////////////////////////////////////

    ///////////////////////######################################################################////////////////////////////////////////
    void incrementalSOJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);

    void incrementalSSJoinStage(AutomataState currState, AutomataState joinState, Dependability dd, Dependability jdd);

    void initialStage(List<AutomataState> aStates, DictionaryOpImpl dic, FinalView[] _FV, long timestamp, long range, long step);

    void ooHashJoinCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2);

    void ooHashJoinRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4);

    ///Issue is here some where
    void ooHashJoinRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4);

    ///////////////////////######################################################################////////////////////////////////////////
    ///Modify it
    void osHashJoinCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2);

    //Edit 4  ..order shit
    void osHashJoinRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC, int order);

    ///Edit 6
    void osHashJoinRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void outputIndex(AutomataState s);

    void outputMaterialsedview(AutomataState s);

    void outputResultsAll(List<AutomataState> a, DictionaryOpImpl dic, FinalView[] _FV, long timestamp, int seq);

    void printOut(DictionaryOpImpl dic);

    ////Something is not right over here
    void refreshStructures(List<AutomataState> aStates);

    void removeOldTriples(List<AutomataState> as, long time, long range, long step);

    void runFirstPhase(Collection<AutomataState> a, DictionaryOpImpl dic, int change, FinalView[] _FV, long timestamp, long range, long step);

    ///o for current state
    void soHashJoinCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2);

    // Edit 3
    void soHashJoinRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4, MultiBiMap<Indexes, Indexes> _pC, MultiBiMap<Indexes, Indexes> _jpC);

    void soHashJoinRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r3, MultiBidirectionalIndex _r4);
    ///////////////////////######################################################################////////////////////////////////////////

    ///Edit 1
    void ssJoinOptimisedForCC(MultiBiMap<Long, Long> _c1, MultiBiMap<Long, Long> _c2, MultiBidirectionalIndex _r1, MultiBidirectionalIndex _r2);

    ///Edit 2
    /// chagen the multi BiMap tp multimap
    void ssJoinOptimisedForRC(MultiBiMap<Long, Long> _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3);

    void ssJoinOptimisedForRC2(MultiBidirectionalIndex _c1, MultiBidirectionalIndex _r2, MultiBidirectionalIndex _r4, MultiBidirectionalIndex _r3);

    void tpJoinAutomataStage(List<AutomataState> automata);

    void tripleJoinStage(AutomataState currState, List<AutomataState> a);

    void tripleRemoval(AutomataState s, long tb);
    
}
