/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.qj.interfaces;

import com.jcwhatever.nucleus.collections.MultiBiMap;
import edu.cgpm.datastructure.FinalView;
import edu.cgpm.datastructure.Indexes;
import edu.cgpm.datastructure.MultiBidirectionalIndex;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.qj.queryJoins.ResultManipulation;
import edu.cgpm.qj.queryJoins.Utilities;
import java.util.List;

/**
 *
 * @author sydgillani
 */
public interface IncQueryProcCycleInterface {

    void case1(List<AutomataState> incList, FinalView[] _FV, DictionaryOpImpl dic, long timestamp, ResultManipulation _reM);


    void case2(List<AutomataState> incList, FinalView[] _FV);

    int getTheCuttingIndex(List<Long> timeList, long tb);

    void incrementalOOJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    void incrementalOSJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    void incrementalSOJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    void incrementalSSJoinStage(AutomataState currState, FinalView tJoin, int caseType);

    void joinWithFV(AutomataState currState, List<AutomataState> a, FinalView[] _FV, int caseType);

    void ooHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void ooHashJoinRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC);

    void ooHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3);

    void osHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void osHashJoinRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4, MultiBiMap<Indexes, Indexes> _pC);

    void osHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4);

    void removalProcess(List<Long> timeList, FinalView _fv, int index);

    void removeMatchesFromFV(List<Long> timeList, long tb, FinalView _fv);

    void run(List<AutomataState> aList, FinalView[] _FV, DictionaryOpImpl dic, long timestamp, long range, long step, Utilities Util, ResultManipulation _reM);

    void setChange(List<AutomataState> incList);
  


    void soHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void soHashJoinRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3, MultiBiMap<Indexes, Indexes> _pC);

    void soHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r3);

    void ssHashJoinCC(MultiBiMap<Long, Long> _c1, FinalView _c2, MultiBidirectionalIndex _r1);

    void ssHashJoinRC2(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4, MultiBiMap<Indexes, Indexes> _pC);

    void ssHashJoinRC2_Case1(MultiBidirectionalIndex _c1, FinalView _r2, MultiBidirectionalIndex _r4);

    void windowUpdate(long range, long step, FinalView[] _FV, long timestamp);
    
}
