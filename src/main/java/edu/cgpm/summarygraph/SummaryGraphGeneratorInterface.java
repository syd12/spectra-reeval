/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package edu.cgpm.summarygraph;

import com.google.common.collect.Multimap;
import edu.cgpm.dictionary.optimised.DictionaryOpImpl;
import edu.cgpm.graph.automata.Automata;
import edu.cgpm.graph.automata.AutomataState;
import edu.cgpm.rulesmodel.Rule;
import org.semanticweb.yars.nx.Literal;
import org.semanticweb.yars.nx.Node;

/**
 *
 * @author sydgillani
 */
public interface SummaryGraphGeneratorInterface {
     public void summaryGraphGeneration(Automata aList, Node[] iTriple,DictionaryOpImpl dic,long timeStamp, Multimap<Long,AutomataState> predmap);
     public void pruningTriples(Automata automata, long _pred, Node[] iTriple,DictionaryOpImpl dic, long timeStamp);
    public boolean pushAtomicFilter(Rule r,Node[] iTriple, DictionaryOpImpl dic);
    public boolean atomicFilterCompariosn(Rule r ,Literal obj ,DictionaryOpImpl dic);
    
    
    
    
    
    
    
}
