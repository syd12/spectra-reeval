# Event-based and Incremental Continuous Graph Pattern Matching

You find here all the necessary materials for the both Event-based and Incremental CGPM. The whole project is distributed into three packages including [`GraphStreamController`](https://bitbucket.org/syd12/streamcontroller/overview), [`GraphQueryParser`](https://bitbucket.org/syd12/queryparser), [`GraphEngineModel`](https://bitbucket.org/syd12/graphenginemodel), [`GraphEngine`](https://bitbucket.org/syd12/graphenginecgpm/overview).
## Installation

First fork all the above mentioned packages into your own account: click on the Fork icon on this page.
Clone the forked project on your computer. Import the project in Eclipse via Import->Maven Project. Please make sure all the packages area added as a dependencies for the main project. 

### Running the system
Three main classes are at your disposal, the first one [`ECGPMEngineSNB`](https://bitbucket.org/syd12/contunuousgpm/src/55d03f36e33a04f237c344bbd633798a393b577a/src/main/java/edu/telecomstet/cep/UI/ECGPMEngineSNB.java?at=default), is used for the even-based CGPM tests (scalability tests) on SNB dataset. The second one [`ECPMEngineNYTaxi`](https://bitbucket.org/syd12/contunuousgpm/src/55d03f36e33a04f237c344bbd633798a393b577a/src/main/java/edu/telecomstet/cep/UI/ECPMEngineNYTaxi.java?at=default) is used for the NY taxi dataset test (performance test). The third one [`IncrementalEngine`](https://bitbucket.org/syd12/contunuousgpm/src/55d03f36e33a04f237c344bbd633798a393b577a/src/main/java/edu/telecomstet/cep/UI/IncrementalEngine.java?at=default) is used for the incremental GPM tests.

## Queries for the test (NY Taxi datasets)

### Query 1


  
    		SELECT * 
                 WHERE  {
                        
                     ?trip1 <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>  ?tripType.
                     ?trip1 <http://example.org/trip_distance> ?trip_distance. 
                     ?trip1 <http://example.org/pickup_datetime> ?pickup_datetime. 
                     ?trip1 <http://example.org/dropoff_datetime> ?dropoff_datetime. 
                     ?trip1 <http://example.org/mta_tax> ?mta_tax.
                     ?trip1 <http://example.org/trip_time_in_secs> ?trip_time_in_secs.  
                        } 
###Query 2

                      SELECT * 
                       WHERE {

                        ?trip <http://example.org/hasTaxiInfo> ?TaxiInfo. 
                        ?TaxiInfo <http://example.org/hasHack_license> ?Hack_license. 
                        ?trip <http://example.org/hasPickupLocation> ?pickUpLocation. 
                        ?pickUpLocation <http://example.org/haspickup_longitude> ?haspickup_longitude. 
                        ?trip <http://example.org/hasFareInfo> ?fareInfo."
                        ?fareInfo <http://example.org/hasFare_amount> ?fare_amount.  
                        }

###Query 3
              SELECT *  
                WHERE {
		       ?trip <http://example.org/hasTaxiInfo> ?TaxiInfo. 
		       ?TaxiInfo <http://example.org/hasHack_license> ?Hack_license. 
		       ?trip <http://example.org/hasPickupLocation> ?pickUpLocation. 
		       ?pickUpLocation <http://example.org/haspickup_longitude> ?haspickup_longitude. 
		       ?trip <http://example.org/pickup_datetime> ?pickup_datetime. 
		        ?trip <http://example.org/dropoff_datetime> ?dropoff_datetime. 
		        }
##Queries for SNB datasets

##Query 1
                         prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> 
                         prefix xsd: <http://www.w3.org/2001/XMLSchema#> 
                         prefix snvoc: <http://www.ldbc.eu/ldbc_socialnet/1.0/vocabulary/> 
                         prefix sn: <http://www.ldbc.eu/ldbc_socialnet/1.0/data/> 
                        
                         prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                        
                         prefix foaf: <http://xmlns.com/foaf/0.1/> 
                         prefix dbpedia: <http://dbpedia.org/resource/> 
 
                         SELECT * WHERE 

                        { 
                         ?post rdf:type snvoc:Post. 
                         ?post snvoc:id ?id. 
                         ?post snvoc:creationDate ?date.
                         ?post snvoc:hasCreator ?creator. 
                         ?post snvoc:hasTag ?tag. 
                         ?post snvoc:isLocatedIn ?loc. 
                         { ///triple patterns for external KB
                         ?loc foaf:name ?locname. 
                         ?tag foaf:name ?tagname. 
                          }
                        }
##Query 2
                        SELECT * WHERE 
                        {  
                         ?forum snvoc:hasCreator ?mod. 
                         ?mod snvoc:likes ?like. 
                         ?like snvoc:hasPost ?post.
                         ?post snvoc:hasTag ?tag. 
                         ?comt snvoc:replyOf ?post. 
                         ?post  snvoc:isLocatedIn  ?loc.
                        { ///triple patterns for external KB
                         ?loc foaf:name ?locname. 
                         ?tag foaf:name ?tagname. 
                         }
                        
                         }
##Query 3

                         SELECT * WHERE 
                         {  
                         ?forum snvoc:hasCreator ?mod. 
                         ?mod snvoc:likes ?like. 
                         ?like snvoc:hasPost ?post. 
                         ?post snvoc:hasCreator ?creator. 
                         ?post snvoc:browserUsed ?bro. 
                         ?post snvoc:isLocatedIn ?loc.
                        {//triple patterns for external KB
                         ?loc foaf:name ?name.
                        } 
                         }
                         

##Queries for LUBM Dataset

### Query 1
                    SELECT ?x ?y ?z WHERE { 
                    ?z ub:subOrganizationOf ?y . 
                    ?y rdf:type ub:University . 
                    ?z rdf:type ub:Department . 
                          ?x ub:memberOf ?z . 
                   ?x rdf:type ub:GraduateStudent . 
                   ?x ub:undergraduateDegreeFrom ?y . 
                        }
###Query 2

                  SELECT ?x WHERE { 
                  ?x rdf:type ub:Course . 
                    ?x ub:name ?y . 
                        }
###Query 3
                     
                  SELECT ?x ?y ?z WHERE { 
                   ?x rdf:type ub:UndergraduateStudent.
                   ?y rdf:type ub:University . 
                  ?z rdf:type ub:Department .
                    ?x ub:memberOf ?z . 
                  ?z ub:subOrganizationOf ?y . 
                ?x ub:undergraduateDegreeFrom ?y . 
                    }
       
###Query 4

              SELECT ?x WHERE {
               ?x ub:worksFor <http://www.Department0.University0.edu> .
             ?x rdf:type ub:FullProfessor . 
             ?x ub:name ?y1 . 
            ?x ub:emailAddress ?y2 .
             ?x ub:telephone ?y3 .
                     }     
###Query 5

               SELECT ?x WHERE { 
               ?x ub:subOrganizationOf <http://www.Department0.University0.edu> .
              ?x rdf:type ub:ResearchGroup
                   }
###Query 6

             SELECT ?x ?y WHERE { 
            ?y ub:subOrganizationOf <http://www.University0.edu> . 
            ?y rdf:type ub:Department . 
            ?x ub:worksFor ?y . 
            ?x rdf:type ub:FullProfessor .
              }   
###Query 7

           SELECT ?x ?y ?z WHERE { 
           ?y ub:teacherOf ?z . 
           ?y rdf:type ub:FullProfessor .
           ?z rdf:type ub:Course . 
           ?x ub:advisor ?y . 
           ?x rdf:type ub:UndergraduateStudent . 
           ?x ub:takesCourse ?z
            }          
                         
##Test datasets
You can find the files for the simple test datasets of [`SNB dataset`](https://bitbucket.org/syd12/contunuousgpm/src/e1f60413be299276597396dbb38009049baab4dd/src/main/java/edu/telecomstet/testData/SocialNetwork-Sample-data.nq?at=default) and [`NYTaxi`](https://bitbucket.org/syd12/contunuousgpm/src/e1f60413be299276597396dbb38009049baab4dd/src/main/java/edu/telecomstet/testData/taxiEvent-Dataset.n3?at=default) dataset containing a set of events.
[`LUBM`](http://swat.cse.lehigh.edu/projects/lubm/) and [`LSBench`](https://code.google.com/p/lsbench/) dataset can be generated from their respective dataset generators.